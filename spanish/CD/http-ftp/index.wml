#use wml::debian::cdimage title="Descarga de imágenes de CD/DVD de Debian mediante HTTP/FTP" BARETITLE=true
#use wml::debian::translation-check translation="0ff5192e787beb3cba84667527045bb299e2b1b2" maintainer="Laura Arjona Reina"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>Por favor, no descargue imágenes de CD o DVD con su navegador web
de la misma forma que descarga otros archivos</strong>. La razón es que
si fracasa la descarga, la mayoría de los navegadores no le permiten
recuperarla desde el punto en el que falló.</p>
</div>

<p>En su lugar, utilice una herramienta que permita la continuación,
típicamente llamada <q>gestor de descargas</q>. Hay muchos complementos de navegadores
que hacen esta tarea, o quizá prefiera instalar un programa independiente.
Por ejemplo puede usar <a
href="https://aria2.github.io/">aria2</a> (Linux, Windows) o <a
href="https://www.downthemall.net/">DownThemAll</a> (complemento del navegador) o (en
línea de órdenes) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> o
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>. Hay
muchas más opciones descritas en esta <a
href="https://es.wikipedia.org/wiki/Anexo:Comparaci%C3%B3n_de_gestores_de_descargas">comparación
de gestores de descarga</a>.</p>

<p>Dispone para descarga directa de las siguientes imágenes:</p>

<ul>

  <li><a href="#stable">Imágenes oficiales de CD/DVD de la versión <q>estable</q></a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Imágenes oficiales de CD/DVD de la distribución <q>en pruebas («testing»)</q> (<em>se
  generan semanalmente</em>)</a></li>

</ul>

<p>Véase también:</p>
<ul>

  <li>Una <a href="#mirrors">lista completa de réplicas de <tt>debian-cd/</tt></a></li>

  <li>Para imágenes de <q>instalación por red</q>,
  mire la página de <a href="../netinst/">instalación por red</a>.</li>
  
  <li>Para imágenes de <q>instalación por red</q> de <q>pruebas («testing»)</q>,
  mire la página del <a href="$(DEVEL)/debian-installer/">instalador de Debian</a>.</li>
</ul>

<hr />

<h2><a name="stable">Imágenes oficiales de CD/DVD de la versión <q>estable</q></a></h2>

<p>Para instalar una máquina sin conexión a Internet, se pueden usar las imágenes
de CD (700&nbsp;MB cada una) o de DVD (4.7&nbsp;GB cada una).
Descargue la primera imagen de CD o DVD, grábela usando una grabadora de CD o DVD 
(o en una memoria USB en las versiones i386 y amd64) y reinicie desde ella.</p>

<p>El <strong>primer</strong> disco CD/DVD contiene todos los archivos necesarios 
para instalar un sistema Debian estándar.<br />
Para evitar descargas innecesarias, por favor, <strong>no</strong> descargue 
otra imagen de CD o DVD a menos que sepa que necesita los paquetes 
que contiene.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Los siguientes enlaces apuntan a archivos de imágenes que ocupan hasta 
700&nbsp;MB, haciéndolos adecuados para grabarse en CD-R(W) normales:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Los siguientes enlaces apuntan a archivos de imágenes que ocupan hasta 4.7&nbsp;GB, 
haciéndolas adecuadas para grabarse en DVD-R/DVD+R normales y similares:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Asegúrese de echarle un vistazo a la documentación antes de instalar.
<strong>Si solo va a leer un documento</strong> antes de instalar, lea nuestro
<a href="$(HOME)/releases/stable/amd64/apa">Cómo instalar</a>, un paseo rápido
por el proceso de instalación. Entre otra documentación útil están:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guía de instalación</a>,
    las instrucciones de instalación detalladas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentación del instalador de Debian</a>
    incluyendo las respuestas a preguntas frecuentes</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Erratas del instalador de Debian</a>,
    la lista de problemas conocidos en el instalador</li>
</ul>

<hr />

<h2><a name="mirrors">Réplicas registradas del archivo de <q>debian-cd</q></a></h2>

<p>Fíjese en que <strong>algunas réplicas pueden no estar actualizadas</strong> &mdash;
la versión actual de las imágenes de CD/DVD es <strong><current-cd-release></strong>.

<p><strong>Si duda, use la <a href="https://cdimage.debian.org/debian-cd/">imagen 
del CD del servidor principal</a> en Suecia.</strong>.</p>

<p>¿Está interesado en ofrecer las imágenes de CD de Debian en
su réplica? Si es así, vea las <a href="../mirroring/">instrucciones acerca de 
cómo configurar una réplica de imágenes de CD de Debian</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
