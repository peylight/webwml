#use wml::debian::translation-check translation="3349f1cfd8d3f3eceed8b216922ec76404aa15f5"
<define-tag pagetitle>DebConf23 i Kochi avslutas och platsen för DebConf24 tillkännages</define-tag>
<define-tag release_date>2023-09-18</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Igår, söndagen 17 september 2023 avslutades den årliga Debian utvecklar- och
bidragslämnarkonferensen.
</p>

<p>
Fler än 474 deltagare som representerade 35 länder från hela världen samlades
för totalt 89 evenemang, bland annat föreläsningar, diskussioner, Birds of a
Feather (BoF)-samlingar, workshops, och aktiviteter för att förbättra vår
distribution, lära av våra mentorer och jämlikar, bygga vår gemenskap, och för
att ha lite kul.
</p>

<p>
Konferensen föregicks av det årliga <a
href="https://wiki.debian.org/DebCamp">DebCamp</a> hackingsessionen som hölls
3 september till 9 september där Debianutvecklare och bidragslämnare
sammankallades för att fokusera sina individuella Debianrelaterade projekt
eller arbeten för att jobba i grupper med fokus på personligt samarbete för
att utveckla Debian.

Särskilt ägde sprintar rum detta år för att främja utvecklingen av
Mobian/Debian, reproducerbara byggen och Python i Debian. Detta år
hade vi även en bootcamp som hölls för nybörjare av en grupp mentorer
som delade med sig av praktisk erfarenhet av Debian och erbjöd en djupare
förståelse för hur man arbetar i och bidrar till gemenskapen.
</p>

<p>
Den faktiska Debianutvecklarkonferensen startade söndagen 10 september 2023.

Utöver det traditionella 'Bitar från DPL'-föreläsningen, den kontinuerliga
nyckelsigneringsfesten, blixtföreläsningar och tillkännagivandet av nästa års
DebConf24, fanns det flera uppdateringssessioner delade av interna projekt
och grupper.

Flera av de hållna diskussionssessionerna presenterades av våra tekniska
grupper som lyfte fram arbetet och fokus för Långtidsstödsgruppen (LTS),
Androidverktyg, Debianderivat, Debianinstalleraren, Debianavbildningar, och
Debians vetenskapsgrupp. Grupperna för programmeringsspråken Python, Perl och
Ruby delade även dom uppdateringar med sig med sitt arbete och sina insatser.

Två av de större lokala Debiangemenskaperna, Debian Brasilien och Debian Indien
delade med sig om hur deras respektive samarbeten i Debian flyttade projektet
framåt och hur de attraherade nya medlemmar och möjligheter både inom
Debian, F/OSS och vetenskap med deras HowTos med demonstrerat
gemenskapsengagemang.
</p>

<p>
<a href="https://debconf23.debconf.org/schedule/">Schemat</a>
uppdaterades varje dag med planerade och ad-hoc-aktiviteter som introducerats
av deltagare under konferensens gång. Flera aktiviteter som inte kunde
hållas under tidigare år på grund av den globala Covid-19-pandemin firades
när de återvände till konferensens schema: en jobbmässa, kvällen med
fri mikrofon, poesiafton, traditionella ost och vin-festen, gruppfoton och
dagsutflykterna.
</p>

<p>
För de som inte har möjlighet att delta är de flesta av föreläsningarna och
sessionerna filmade för live-strömmar vilka görs tillgängliga senare genom
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/">arkivet för Debianmöten</a>.
Nästan alla sessioner tillät fjärrdeltagande via IRC-meddelandeappar eller
samarbetstextdokument vilket tillät fjärrdeltagare att "vara i rummet" för att
ställa frågor eller dela kommentarer med talaren och den samlade publiken.
</p>

<p>
DebConf23 såg över 4.3 TiB strömmade data, 55 timmar schemalagda föreläsningar,
23 nätverksåtkomstpunkter, 11 nätverksswitchar, 75 kg importerad utrustning, 400
meter gaffatejp, 1463 visade strömmade timmar, 461 t-shirts, Geoip-tittare från
35 länder, 5 dagsutflykterna, och ett genomsnitt på 169 planerade måltider per
dag.

Alla dessa evenemang, aktiviteter, konversationer och strömmar tillsammans
med vår kärlek, vårt intresse och deltagande i Debian och F/OSS gjorde
absolut denna konferens till en succé både här i Kochi, Indien och online
runt hela världen.
</p>

<p>
<a href="https://debconf23.debconf.org/">Webbplatsen för DebConf23</a>
kommer att hållas aktiv för arkiveringsändamål och kommer att fortsätta att
erbjuda länkar till presentationerna och videos av föreläsningarna och
evenemangen.
</p>

<p>
Nästa år kommer <a href="https://wiki.debian.org/DebConf/24">DebConf24</a> att
hållas i Haifa, Israel. Enligt traditionen kommer lokala organisatörer före
DebConf starta konferensaktiviteterna med DebCamp med särskilt fokus på
individuellt arbete och grupparbete för förbättring av distributionen.
</p>

<p>
DebConf arbetar för en säker och välkomnande miljö för alla deltagare.
Se <a href="https://debconf23.debconf.org/about/coc/">webbsidan om förhållningsregler på webbplatsen för DebConf23</a>
för ytterligare detaljer om detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf23.debconf.org/sponsors/">sponsorer</a>
för deras stöd för DebConf23, speciellt våra Platinasponsorer:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>,
och <a href="https://www.siemens.com/">Siemens</a>.
</p>

<p>
Vi vill även tacka våra video- och infrastrukturgrupper, DebConf23- och
DebConf-kommittéerna vår värdnation Indien och varje enstaka person som
hjälpte till att bidra till detta evenemang eller Debian i allmänhet.

Tack till er alla för ert arbete med att hjälpa Debian att fortsätta vara
"det universella operativsystemet".

Vi ses nästa år!
</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella
	operativsystemet</q>.
</p>


<h2>Om DebConf</h2>

<p>DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullspäckat
schema med tekniska, sociala och policytal, tillhandahåller DebConf en
möjlighet för utvecklare, bidragslämnare och andra intresserade att
mötas personligen och jobba tillsammans. Det är ägt rum årligen sedan
2000 på så vitt skilda platser som Skottland, Argentina, och
Bosnien och Herzegovina. Mer information om DebConf finns tillgänglig
på <a href="http://debconf.org/">http://debconf.org</a>.</p>


<h2>Om Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är en nyckelspelare i
Europeiska molnmarknaden och den ledande utvecklaren av webbteknologier i
Schweiz. De har målet att vara ett obroende europeiskt alternativ till
webbgiganterna och är hängiven till en etiskt och hållbar webb som respekterar
integritet och skapar lokala arbetsmöjligheter. Infomaniak utvecklar
molnlösningar (IaaS, PaaS, VPS), produktivitetsverktyg för onlinesamarbete och
video- och radioströmningstjänster.
</p>

<h2>Om Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> utvecklar kraftfull men
ändå lättanvänd servermjukvara med öppen källkod. Produktporföljen från
Proxmox, inklusive servervirtualisering, backup, och e-postsäkerhet, hjälper
företag oavsett storlek, sektor eller branch att förenkla sina
IT-infrastrukturer. Proxmox-lösningarna baseras på den fantastiska
Debianplatformen och vi är nöjda att vi kan ge tillbaka till gemenskapen genom
att sponsra DebConf23.
</p>

<h2>Om Siemens</h2>
<p>
<a href="https://www.siemens.com/">Siemens</a> är ett teknoligiföretag
med fokus på industri, infrastruktur och transport. Från resurseffektiva
fabriker, motståndskraftiga leveranskedjor, smartare byggnader och nät, till
renare och mer bekväma transporter och avanverad sjukvård skapar företaget
teknologi med syfte att tillföra verkligt värde till kunderna. Genom att kombinera
verkliga och digitala världar ger Siemens sina kunder möjlighet att transformera
sina branscher och marknader, vilket hjälper dem att förbättra vardagen
för miljarder människor.
</p>


<h2>Kontaktinformation</h2>

<p>För ytterligare information, var vänlig besök DebConf23s webbsida på
<a href="https://debconf23.debconf.org/">https://debconf23.debconf.org/</a>
eller skicka e-post (på engelska) till &lt;press@debian.org&gt;.</p>
