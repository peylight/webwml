#use wml::debian::template title="Hvad betyder fri?" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Fri som i ...?</a></li>
    <li><a href="#licenses">Softwarelicenser</a></li>
    <li><a href="#choose">Hvordan vælges en licens?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> I februar 1998 forsøgte en 
gruppe at erstatte udtrykket <a href="https://www.gnu.org/philosophy/free-sw">\
fri software</a> med <a href="https://opensource.org/osd">\
open source-software</a>.  Terminologidebatten afspejler de underliggende 
filosofiske forskelle, men de praktiske krav foruden andre emner, som disktueres 
her på stedet, er grundlæggende de samme for fri software og open 
source-software.</p>
</aside>


<h2><a id="freesoftware">Fri som i ...?</a></h2>

<p>Mange personer, som ikke er vant til fri software, bliver forvirrende på 
grund af ordet <q>fri</q> (<q>free</q> på engelsk).  Det anvendes ikke på den 
måde, som de forventer – <q>fri</q> betyder for dem <q>omkostningsfri</q>.  Hvis 
man kigger i en engelsk ordbog, opremses der næsten tyve forskellige betydninger 
for <q>free</q>, hvor kun en enkelt af dem er <q>omkostningsfri</q>.  De øvrige 
refererer til <q>frihed</q> og <q>ingen tvang</q>.  Når vi taler om fri software, 
mener vi frihed og ikke betaling.  På dansk er det mindre kompliceret, da vi 
normalt anvender ordet <q>gratis</q>, når man taler om at noget er 
<q>omkostningsfrit</q>.</p>

<p>Software, der beskrives som fri, men kun i betydningen af at man ikke behøver 
at betale for den, er næppe fri når det kommer til stykket.  Måske er det 
forbudt at give softwaren videre, og man har næsten helt sikkert ikke lov til at 
ændre den.  Software, som licenseres uden omkostninger, er normalt et element i 
en markedsføringskampagne, for at fremhæve et relateret produkt eller for at få 
mindre konkurrenter ud af markedet.  Der er ingen garanti for at softwaren vil 
forblive gratis.</p>

<p>For den uindviede er et stykke software enten frit eller ikke frit 
tilgængeligt.  Virkelighedens verden er mere kompliceret end som så.  For at 
forstå hvad folk mener, når de taler om fri software, må vi tage en lille 
afstikker ind i softwarelicensernes verden.</p>


<h2><a id="licenses">Softwarelicenser</a></h2>

<p>Ophavsretslig beskyttelse (<q>copyright</q> på engelsk) er en måde at 
beskytte udviklerens interesser på, ved visse former for værker.  I de fleste 
lande er nyudviklet software automatisk beskyttet af ophavsret.  En licens er 
forfatterens måde at tillade andres brug af dennes værk (software i dette 
tilfælde), på måder som vedkommende kan acceptere.  Det er op til forfatteren, 
at medlevere en licens, der angiver på hvilken måde softwaren må benyttes.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Læs mere om ophavsret</a></button></p>

<p>Naturligvis kræver forskellige omstændigheder forskellige licenser.
Softwarevirksomheder søger efter måder at beskytte deres værdier på, hvorfor de 
ofte kun udgiver kompileret kode, der ikke kan læses af mennesker.  De lægger 
også mange restriktioner på brugen af softwaren.  På den anden side fokuserer 
forfattere af fri software primært på forskellige regler, nogle gange endda en 
kombination af følgende punkter:</p>

<ul>
  <li>Det er ikke tilladt at anvende deres kode i proprietær software.  Da de 
      udgiver deres software, så alle må anvende den, ønsker de ikke at andre 
      stjæler den.  I den situation ses anvendelse af software som en 
      tillidssag:  Man må anvende den, såfremt man spiller efter de samme 
      regler.</li>
  <li>Forfatterskabets identitet skal beskyttes.  Folk er stolte over deres 
      arbejde og ønsker ikke at andre fjerner deres navne fra det, eller endda 
      hævder at de selv har skrevet den.</li>
  <li>Kildekode skal distribueres.  Et stort problem med proprietær software er 
      at man ikke kan rette fejl eller tilpasse den, da kildekoden ikke er 
      tilgængelig.  Desuden kan en leverandør beslutte, ikke længere at 
      understøtte brugernes hardware.  Distribution af kildekode, som de fleste 
      fri licenser har krav om, beskytter brugerne ved at tillade, at de 
      tilpasser softwaren og justerer den til deres behov.</li>
  <li>Ethvert arbejde, som indeholder dele af forfatterens arbejde (også kaldet 
      <q>afledte værker</q> i ophavsretdiskussioner), skal anvende den samme 
      licens.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Tre af de mest udbredte fri 
software-licenser er 
<a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>, 
<a href="https://opensource.org/blog/license/artistic-2-0">Artistic License</a> og 
<a href="https://opensource.org/blog/license/bsd-3-clause">BSD Style License</a>.
</aside>


<h2><a id="choose">Hvordan vælges en licens?</a></h2>

<p>Nogle gange skriver folk deres egne licenser, hvilket kan være problematisk, 
og er ikke vellidt i fri software-fællesskabet.  Alt for ofte er formuleringerne 
enten tvetydige eller folk fastsætter betingelser, som er i konflikt med 
hinanden.  At skrive en licens, der holder i byretten, er endnu sværere.  
Heldigvis er der en række open source-licenser, som man kan vælge mellem.  De 
har følgende fælles kendetegn:
</p>

<ul>
  <li>Brugerne kan installere softwaren på så mange maskiner, som de ønsker.</li>
  <li>Et ubegrænset antal personer må anvende softwaren på samme tid.</li>
  <li>Brugerne kan lave så mange kopier af softwaren, som de ønsker eller har 
      brug for, samt give disse til andre brugere (fri eller åben 
      videregivelse).</li>
  <li>Der er ingen begrænsninger på ændring af softwaren (bortset fra at bevare 
      visse krediteringer).</li>
  <li>Brugerne kan ikke kun videregive softwaren, de kan også sælge den.</li>
</ul>

<p>Særligt det sidste punkt, der tillader at folk sælger softwaren, kunne lade 
til at være imod hele ideen med fri software, men det er i virkeligheden en af 
dens styrker.  Da licensen tillader fri videregivelse, kan den videregives når 
nogen får en kopi af den.  Folk kan endda prøve at sælge den.</p>

<p>Mens fri software ikke er helt fri for begrænsninger, giver det brugerne den 
fleksibilitet til at gøre, hvad de har brug for, for at få udført opgaven.  På 
samme tid beskytter det forfatterens rettigheder – det er virkelig frihed.
Debian-projektet og dets medlemmer går stærkt ind for fri software.  Vi har 
udformet <a href="../social_contract#guidelines">Debians retningslinjer for 
fri software (DFSG)</a>, for have en fornuftig definition af hvad fri software 
er efter vores mening.  Kun software, som lever op til DFSG, er tilladt 
i vores primære Debian-distribution.</p>
