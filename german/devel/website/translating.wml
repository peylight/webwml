#use wml::debian::template title="Debian-Webseiten übersetzen" BARETITLE=true
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="8627b3f6f68ecae195c89923923955f8e32ea092"
# $Id$
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<p>
Um die Arbeit für die Übersetzer so einfach wie möglich zu machen, werden
die Seiten etwas anders generiert als viele, mit denen Sie vertraut sind. Die
Webseiten werden letztlich aus Quellcode generiert, der mit
<a href="https://www.shlomifish.org/open-source/projects/website-meta-language/"><tt>wml</tt></a> ausgezeichnet
ist. Es gibt für jede Sprache ein eigenes Verzeichnis.
</p>

<p>
Wenn Sie eine komplett neue Übersetzung der Debian-Webseiten erstellen
wollen, lesen Sie bitte den <a href="#completenew">Abschnitt über das Beginnen
einer neuen Übersetzung</a>.
</p>

# Achtung: Der folgende Absatz steht nicht in dem englischen Original.
# Es ist derzeit aber wichtig, unseren Hilfeaufruf prominent zu
# platzieren, um neue Leute anzuwerben. Diese Seite scheint dafür
# thematisch sehr gut geeignet zu sein. Wenn wieder viele Leute
# mithelfen, können wir den Absatz entfernen.
# Siehe auch german/international/german/index.wml
# Tobias Quathamer, 12.10.2009
<p>
<em>Wichtig: Die deutschen Übersetzer der Website <a
href="https://lists.debian.org/debian-l10n-german/2008/09/msg00023.html">benötigen
Hilfe</a>, um die Übersetzung aktuell zu halten und neue Inhalte
(z.B. Nachrichten) zu übersetzen. Falls Sie helfen möchten, die Website
zu übersetzen, lesen Sie bitte auch die
<a href="$(HOME)/international/german/">Seite über deutschsprachige
Unterstützung für Debian</a>.</em>
</p>


<h2><a name="singlepages">Einzelne Seiten übersetzen</a></h2>

<p>
Wir verwenden WML, um den speziellen Inhalt einer Seite von gemeinsamen
Elementen für mehrere Seiten zu trennen. Dies bedeutet, dass man bestimmte
WML-Quellcode-Dateien anstelle von HTML-Dateien ändern muss.
Bitte <a href="using_git">Verwenden Sie
Git</a>, um sich die aktuellen Quelldateien zu besorgen. Sie müssen zumindest
zwei Verzeichnisse auschecken: <tt>webwml/english/</tt> und
<tt>webwml/<var>&lt;sprache&gt;</var>/</tt>.
</p>

<p>
Um eine einzelne Seite von Englisch in Ihre Sprache zu übersetzen, muss
die original .wml-Datei übersetzt und im Verzeichnis der anderen Sprache
abgelegt werden. Der relative Pfad und Name muss der gleiche wie im
englischen Verzeichnis sein, damit die Links weiterhin funktionieren.
</p>


<h3>Übersetzungs-Kopfzeilen</h3>

<p>
Es wird
dringend empfohlen, dass der Übersetzer eine zusätzliche Zeile nach der
letzten <code>#use</code> Anweisung hinzufügt, um den exakten Commit der
Original-Datei festzuhalten, gemäß dem übersetzt wurde, damit das
<a href="uptodate">Aktualisieren einfacher ist</a>. Die Zeile sieht wie folgt
aus:
<kbd>#use wml::debian::translation-check translation="<var>&lt;git_commit_hash&gt;</var>"</kbd>
Bitte beachten Sie: wenn Sie die zu übersetzende Datei mit dem Hilfsprogramm
<tt>copypage.pl</tt> erzeugen, (was wir dringend empfehlen,) wird der
korrekte Commit-Hash-Wert automatisch eingefügt. Die Verwendung von
<tt>copypage.pl</tt> wird im Folgenden erklärt.
</p>

<p>
Einige Übersetzungsteams verwenden diese Zeile ebenfalls, um den
offiziellen Übersetzer für die jeweilige Webseite festzuhalten. Falls Sie dies tun,
erhalten Sie automatisch eine E-Mail, wenn die Seiten, die Sie betreuen, im
Original aktualisiert wurden, und die Übersetzung Ihre Aufmerksamkeit zum
Aktualisieren benötigt. Dafür fügen Sie einfach Ihren Namen als Betreuer am
Ende der <code>#use</code> Zeile hinzu, so dass diese wie folgt aussieht:
<kbd>#use wml::debian::translation-check translation="<var>git_commit_hash</var>" maintainer="<var>Ihr Name</var>"</kbd>
Das Skript <tt>copypage.pl</tt> führt dies automatisch durch, wenn Sie die
Umgebungsvariable <tt>DWWW_MAINT</tt> setzen oder den Befehlszeilenschalter
<tt>-m</tt> verwenden.
</p>

# I Removed cvs specific descriptions from here because of cvs to git transition.
# Help to update instruction if possible.
#
#<p>Sie müssen dem Roboter auch erzählen, wer Sie sind, wie oft Sie
#automatische E-Mails erhalten wollen, und was darin enthalten sein soll. Dafür
#editieren Sie (oder lassen es von Ihrem Koordinator editieren) die Datei
#<tt>webwml/<var>sprache</var>/international/<var>sprache</var>/translator.db.pl</tt>
#im CVS. Die Schreibweise sollte gut verständlich sein, und Sie können die
#Datei des französischen Teams als Vorlage verwenden, falls sie für Ihre
#Sprache noch nicht vorhanden ist. Der Roboter kann verschiedene Arten von
#Informationen verschicken, und für jede davon können Sie die Häufigkeit
#angeben, in der sie an Sie geschickt werden. Die verschiedenen Arten der
#Informationen sind:</p>
#
#<ul>
#  <li><b>summary</b>: Eine Zusammenfassung, welche Dokumente veraltet
#    sind.</li>
#  <li><b>logs</b>: Das <q>cvs log</q> zwischen den übersetzten und aktuellen
#    Versionen.</li>
#  <li><b>diff</b>: <q>cvs diff</q></li>
#  <li><b>tdiff</b>: Das Skript wird versuchen, den Teil im übersetzten Text zu
#    finden, der vom englischen Patch geändert wurde.</li>
#  <li><b>file</b>: Fügt die aktuelle Version der Datei hinzu, die übersetzt
#    werden soll.</li>
#</ul>
#
#<p>Der Wert für jedes davon sollte eines von den folgenden sein: 0 (niemals),
#1 (monatlich), 2 (wöchentlich) oder 3 (täglich).</p>
#
#<p>Ein Beispiel könnte sein:</p>
#
#<verbatim>
#                'Martin Quinson' => {
#                        email       => 'martin.quinson@tuxfamily.org',
#                        summary     => 3,
#                        logs        => 3,
#                        diff        => 3,
#                        tdiff       => 0,
#                        file        => 0
#                },
#</verbatim>

<p>
Die Kopfzeilen der Webseite können leicht erstellt werden, indem man das
<tt>copypage.pl</tt>-Skript aus dem webwml-Wurzelverzeichnis verwendet. Das
Skript wird die Seite an den richtigen Platz kopieren, Verzeichnisse und
Makefiles falls notwendig erstellen, und die notwendige Kopfzeile automatisch
hinzufügen. Sie erhalten eine Warnung, falls die zu kopierende Datei bereits
im Depot existiert, entweder weil die Datei aus dem Depot entfernt wurde (weil
sie zu veraltet war) oder weil jemand bereits eine Übersetzung kopiert hat und
ihre lokale Kopie nicht aktuell ist.
</p>

<p>Bevor Sie <tt>copypage.pl</tt> verwenden, sollten Sie zuerst die Datei
   <tt>language.conf</tt> im <tt>webwml</tt>-Wurzelverzeichnis konfigurieren,
   die vom Skript benutzt wird, um die Sprache zu bestimmen, in die Sie übersetzen.
   Diese Datei benötigt bis zu zwei Zeilen: die erste Zeile muss zwingend den
   Namen der Sprache enthalten (z.B. <tt>german</tt>) und in der zweiten können
   Sie optional den Namen des Übersetzers (Ihren Namen) angeben.
   Sie haben auch die Möglichkeit, die Sprache über die Umgebungsvariable
   <tt>DWWW_LANG</tt> zu setzen sowie den Namen über <tt>DWWW_MAINT</tt>,
   so dass dieser in den Kopfzeilen der erstellten WML-Dateien als Betreuer
   dieser Übersetzung eingetragen wird. 
   Und die dritte Möglichkeit ist, <tt>-l german -m "Donald Duck"</tt> auf
   der Kommandozeile anzugeben, um Sprache und Name zu setzen.
   Es gibt auch noch weitere Funktionalitäten in diesem Skript, führen Sie es
   einfach ohne Optionen aus, um Hilfe zu erhalten.
</p>

<p>
Nachdem Sie z.B. <kbd>./copypage.pl <var>datei</var>.wml</kbd> aufgerufen
haben, übersetzen Sie den englischen Text in der Datei. Kommentare in den
Dateien zeigen an, ob es Teile gibt, die nicht übersetzt werden sollten;
respektieren Sie diese. Führen Sie keine unnötigen Änderungen an der
Formatierung durch; falls Sie etwas finden, von dem Sie denken, dass es
geändert/korrigiert werden sollte, muss dies wahrscheinlich auch in
der (englischen) Original-Datei geändert werden.
</p>


<h3>Übersetzen und Veröffentlichen der Seiten</h3>

<p>
Da wir <a href="content_negotiation">Inhalts-Aushandlung</a> verwenden,
sind HTML-Dateien nicht im Format <tt><var>datei</var>.html</tt> benannt, sondern
<tt><var>datei</var>.<var>&lt;sprache&gt;</var>.html</tt>, wobei
<var>&lt;sprache&gt;</var> der zweizeichige Code der Sprache ist, entsprechend
<a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639</a>
(z.B. <tt>de</tt> für Deutsch).
</p>

<p>
Sie können aus WML HTML erzeugen, indem Sie
<kbd>make <var>datei</var>.<var>&lt;sprache&gt;</var>.html</kbd> aufrufen.
Falls es funktioniert, prüfen Sie, ob die Syntax komplett gültig ist, indem
Sie <kbd>weblint <var>datei</var>.<var>&lt;sprache&gt;</var>.html</kbd>
verwenden.
</p>

<p>
HINWEIS: Die Webseiten werden auf dem www-master Server regelmäßig neu
generiert, basierend auf den wml-Quelldateien in Git. Dieser Prozess ist
größtenteils nicht anfällig für Fehler. Falls Sie
jedoch eine kaputte Datei im Hauptverzeichnis Ihrer Übersetzung übergeben
(z.B. die index.wml-Datei in /webwml/german/), wird dies den
Übersetzungsprozess ruinieren und alle anderen Aktualisierungen für die
Website zum Stillstand bringen. Seien Sie bitte besonders vorsichtig bei
solchen Dateien.
</p>

<p>
Wenn die Seite in Ordnung ist, können Sie sie ins Git übergeben. Falls Sie die
Berechtigungen haben, dies selbst zu tun, nehmen Sie den Commit in das
<a href="https://salsa.debian.org/webmaster-team/webwml">webwml
Git-Repository</a> selbst vor; falls nicht, schicken Sie ihn an
<a href="translation_coordinators">jemanden mit Schreibberechtigung für
das Depot</a>.
</p>


<h2><a name="completenew">Eine neue Übersetzung beginnen</a></h2>

<p>
Wenn Sie beginnen wollen, die Debian-Webseiten in eine neue Sprache zu
übersetzen, schicken Sie uns eine E-Mail (auf Englisch) an
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a>.
</p>

<p>
Zu allererst besorgen Sie sich die Quelldateien, wie auf unserer <a
href="using_git">Git-Einführungsseite</a> beschrieben.
</p>

<p>
Wenn Sie den git-Checkout vorliegen haben, erzeugen Sie ein neues
Hauptverzeichnis für Ihre Sprache, neben /english/ und den anderen. Der
Name des Übersetzungs-Verzeichnisses muss in Englisch und komplett
kleingeschrieben sein (z.B. <q>german</q>, nicht <q>Deutsch</q>).
</p>

<p>
Kopieren Sie die <tt>Make.lang</tt> und <tt>.wmlrc</tt> Dateien aus dem
english/-Verzeichnis in das neue Übersetzungs-Verzeichnis. Diese Dateien sind
essentiell zum Bauen Ihrer Übersetzung der WML-Dateien. Sie wurden so
gestaltet, dass Sie nach dem Kopieren in das neue Sprach-Verzeichnis nur diese
Dinge ändern müssen:
</p>

<ol>
  <li>Die Variable LANGUAGE muss in der <tt>Make.lang</tt> Datei
      geändert werden.</li>

  <li>Die Variable CUR_LANG, CUR_ISO_LANG und CHARSET müssen in der
      <tt>.wmlrc</tt> Datei geändert werden. Fügen Sie
      CUR_LOCALE hinzu, falls Sie es für die Sortierung benötigen.</li>

  <li>Einige Sprachen könnten zusätzliche Änderungen benötigen, um den
      Zeichensatz festzulegen. Dies kann über die --prolog und --epilog
      Optionen von wml geschehen. Verwenden Sie die WMLPROLOG und WMLEPILOG
      Variablen in <tt>Make.lang</tt>, um dies zu tun.</li>

  <li>Die Variable LANGUAGES muss in der <tt>webwml/Makefile</tt> Datei in der
      obersten Ebene geändert
      werden, damit Ihre Sprache gemeinsam mit den anderen auf www.debian.org
      übersetzt wird. Wir würden es bevorzugen, wenn Sie diese spezielle
      Änderung den Webmastern überlassen, da es Ihnen nicht bewusst sein
      dürfte, dass Ihre Übersetzung kaputt ist, wenn Sie frisch aus dem VCS (Git)
      ausgecheckt wird, was den Übersetzungsprozess für den Rest unserer
      Website negativ beeinflussen kann.</li>
</ol>

<p>
Nachdem dies getan ist, fügen Sie die folgende Zeile in eine neue Datei
namens <q>Makefile</q> in das Verzeichnis ein:
</p>

<pre>
<protect>include $(subst webwml/<var>ihrsprachverzeichnis</var>,webwml/english,$(CURDIR))/Makefile</protect>
</pre>

<p>
(Sie müssen natürlich <var>ihrsprachverzeichnis</var> mit dem Namen Ihres
Sprach-Verzeichnisses ersetzen.)
</p>

<p>
Erstellen Sie nun ein Unterverzeichnis in Ihrem Sprach-Verzeichnis namens
<q>po</q>, und kopieren Sie das gleiche Makefile in dieses Unterverzeichnis
(<kbd>cp ../Makefile .</kbd>).
</p>

<p>
Im po/-Verzeichnis rufen Sie <kbd>make init-po</kbd> auf, um den
anfänglichen Satz von *.po Dateien zu erstellen.
</p>

<p>
Da Sie damit das Skelett aufgesetzt haben, können Sie damit beginnen, Ihre
Übersetzungen in unseren verteilten WML-Tags hinzuzufügen, die in den Vorlagen
verwendet werden. Die ersten Vorlagen, die Sie übersetzen sollten, sind jene,
die auf allen Webseiten auftauchen, wie die Kopfzeilen-Schlüsselwörter, die
Einträge in der Navigationszeile und die Fußzeile.
</p>

# Die Seite über die <a href="using_wml">Verwendung von WML</a> hat weitere
# Informationen darüber.

<p>
Fangen Sie mit der Übersetzung in der
<code>po/templates.<var>xy</var>.po</code> Datei an (wobei <var>xy</var> für
Ihren zweistelligen Sprach-Code steht). Für jede <code>msgid
"<var>something</var>"</code> gibt es anfänglich eine <code>msgstr ""</code>
Zeile, in die Sie die Übersetzung von <var>something</var> zwischen den
Anführungszeichen nach <code>msgstr</code> einfügen sollten.
</p>

<p>
Sie müssen nicht alle Textteile in allen .po-Dateien übersetzen, nur jene,
die Ihre aktuell übersetzten Seiten tatsächlich benötigen. Um zu sehen, ob Sie
einen Textteil übersetzen müssen, lesen Sie den Kommentar in der .po Datei
gleich über jeder <code>msgid</code> Anweisung. Falls sich die referenzierte
Datei in <tt>english/template/debian</tt> befindet, sollten Sie ihn mit hoher
Wahrscheinlichkeit übersetzen. Falls nicht, können Sie es für später
aufschieben, wenn Sie schlussendlich den relevanten Teil der Webseiten
übersetzen, der es benötigt.
</p>

<p>Das Ziel der po/-Dateien ist, das Leben für die Übersetzer zu erleichtern,
  so dass sie (fast) nie etwas in dem Verzeichnis 
  <tt>english/template/debian</tt> direkt bearbeiten müssen. Falls Ihnen 
  irgendein Fehler mit irgendeinem Detail der Installation im 
  template-Verzeichnis auffällt, stellen Sie sicher, dass die Korrektur des
  Problems generisch ist (scheuen Sie sich nicht, jemanden anders zu bitten,
  die Korrektur für Sie vorzunehmen). Pflegen Sie nicht die eigentliche 
  Übersetzung in die Vorlagen ein, da dies (normalerweise) zu einem großen 
  Problem führt.</p>

<p>
Falls Sie sich nicht sicher sind, ob Sie etwas richtig gemacht haben,
fragen Sie auf der debian-www-Mailingliste, bevor Sie die Datei ins
Depot übergeben.
</p>

<p>
Beachten Sie: Wenn Sie es für notwendig empfinden, weitere Änderungen
durchzuführen, schicken Sie eine E-Mail an debian-www mit dem Hinweis, was Sie
warum geändert haben, damit das Problem behoben werden kann.
</p>

<p>
Nachdem das Vorlagen-Skelett fertig ist, können Sie mit der Übersetzung der
Hauptseite und den anderen *.wml-Dateien beginnen. Für eine Liste
der Dateien, die Sie zuerst übersetzen sollten, prüfen Sie
<a href="translation_hints">die Hinweis-Seite</a>. Übersetzen Sie die
*.wml-Dateien, wie es <a href="#singlepages">am Anfang dieser Seite</a>
beschrieben ist.
</p>

<h3>Veraltete Übersetzungen wiederbeleben</h3>

<p>Wie in <q><a href="uptodate">Website-Übersetzungen aktuell halten</a></q> 
   beschrieben, können veraltete Übersetzungen automatisch von der Website
   entfernt werden, wenn eine längere Periode ohne Aktualisierung verstrichen
   ist.
</p>

<p>Falls Sie feststellen, dass Dateien irgendwann in der Vergangenheit gelöscht
   wurden, und Sie diese Dateien für weiteres Editieren nochmals öffnen
   möchten, können Sie mit Standard-git-Befehlen die commit-History durchsuchen.
</p>

<p>Wenn die gelöschte Datei z.B. <q>deleted.wml</q> hieß, können Sie die History
   danach durchsuchen, indem Sie folgendes ausführen:
</p>

<verbatim>
   git log --all --full-history -- <path/to/file/deleted.wml>
</verbatim>

<p>Sie können so den exakten Commit finden, in dem die Datei entfernt wurde,
   zusammen mit dem Commit-Hash-Wert. Um die Detailinformationen anzuzeigen,
   die zu diesen Änderungen an der Datei hinterlegt sind, führen Sie wie
   folgt einen <code>git show</code>-Befehl aus:
</p>

<verbatim>
   git show <COMMIT_HASH_STRING> -- <path/to/file/deleted.wml>
</verbatim>

<p>Wenn der Commit exakt derjenige ist, der die Datei gelöscht hat, können
   Sie die Datei mittels <code>git checkout</code> in den Arbeitsbereich
   wiederherstellen:
</p>

<verbatim>
   git checkout <COMMIT_HASH_STRING>^ -- <path/to/file/deleted.wml>
</verbatim>

<p>Sobald Sie dies durchgeführt haben, müssen Sie natürlich das Dokument
   aktualisieren, bevor Sie es wieder einpflegen. Andernfalls könnte es erneut
   entfernt werden.
</p>


<h3>Der Rest der Geschichte</h3>

<p>Die obige Beschreibung wird wahrscheinlich ausreichend sein, um Ihnen den
Anfang zu ermöglichen. Später möchten Sie eventuell noch die folgenden Dokumente
lesen, die detailliertere Erklärungen und zusätzliche nützliche Informationen
beinhalten:
</p>

<ul>
<li>Einige verfügbare <a href="examples">Beispiele</a>, die Ihnen eine klarere
    Idee bieten, wie Sie anfangen sollen.</li>
<li>In der <a href="translation_hints">Übersetzungsvorschläge</a>-Seite sind
    einige allgemeine Fragen beantwortet und nützliche Vorschläge 
    vorhanden.</li>
<li>Wir haben Mechanismen, die dabei helfen, <a href="uptodate">die
    Übersetzungen aktuell zu halten</a>.</li>
<li>Um den Status Ihrer Übersetzung zu sehen und zu schauen, wie diese sich
    mit anderen vergleicht, prüfen Sie die <a href="stats/">\
    Statistiken</a>.</li>
</ul>

<p>Wir hoffen, dass Sie die Arbeit, die wir geleistet haben, als eine Hilfe
für das Übersetzen ansehen. Wie bereits erwähnt wurde können Sie alle Fragen,
die Sie haben, auf der <a href="mailto:debian-www@lists.debian.org">\
debian-www</a>-Mailingliste ansprechen.</p>
