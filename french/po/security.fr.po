# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: debian webwml security 0.1\n"
"PO-Revision-Date: 2008-07-28 21:29+0200\n"
"Last-Translator: Pierre Machard <pmachard@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Sécurité Debian"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Bulletins d'alerte Debian"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "Question <define-tag small_Q>Q. </define-tag>"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"annonces de sécurités <a href=\\\"undated/\\\">non datées</a>, conservées "
"pour la postérité"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Dictionnaire CVE du Mitre&nbsp;:"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Base de données du suivi des bogues chez Securityfocus"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "Alertes du CERT"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "Notes d'alerte du CERT US"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Source :"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Composant indépendant de l'architecture&nbsp;:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"Les sommes MD5 des fichiers indiqués sont disponibles sur la <a href=\"<get-"
"var url />\">page originale de l'alerte de sécurité</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"Les sommes MD5 des fichiers indiqués sont disponibles dans la <a href=\"<get-"
"var url />\">nouvelle annonce de sécurité</a>."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "Bulletin d'alerte Debian"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "Date du rapport "

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "Paquets concernés "

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "Vulnérabilité "

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "Références dans la base de données de sécurité "

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "Plus de précisions "

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "Corrigé dans "

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "Identifiant BugTraq"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Bogue"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "Dans le système de suivi des bogues Debian&nbsp;:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr ""
"Dans la base de données de suivi des bogues (chez SecurityFocus)&nbsp;:"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "Dans le dictionnaire CVE du Mitre&nbsp;:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "Les annonces de vulnérabilité et les bulletins d'alerte du CERT&nbsp;:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
"Aucune référence à une base de données externe en rapport avec la sécurité "
"n'est actuellement disponible."
