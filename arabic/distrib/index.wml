#use wml::debian::template title="الحصول على دبيان"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="1e20948729cab8fb8cdb0184c8cdb472633340a0"

<p>تعرض هذه الصفحة خيارات تثبيت الإصدارة المستقرة لدبيان.


<ul>
<li> <a href="../CD/http-ftp/#mirrors">مرايا تنزيل</a> صور التثبيت
<li> <a href="../releases/stable/installmanual">دليل التثبيت</a> مع إرشادات مفصّلة
<li> <a href="../releases/stable/releasenotes">ملحوظات الإصدار</a>
<li> <a href="../releases/">إصدارات أخرى</a> مثل الاختبارية وغير المستقرة
</ul>
</p>


<div class="line">
  <div class="item col50">
    <h2><a href="netinst">تنزيل صورة لوسيط التثبيت</a></h2>
    <ul>
      <li><a href="netinst"><strong>صور التثبيت صغيرة الحجم</strong></a>:
	    تنزيلها سريع وتسجّل على وسيط قابلة للإزالة.
	    استخدامها يتطلب توفر اتصال إنترنت على الجهاز.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC netinst torrents</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC netinst torrents</a></li>
	</ul>
      </li>
      <li><a href="../CD/"><strong>صورة تثبيت كاملة</strong></a> أكبر حجما: 
        تحتوي حزما أكثر، تسهّل التثبيت على الحواسيب التي ﻻ تتوفر على اتصال إنترنت.
	<ul class="quicklist downlist">
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">جرّب دبيان live قبل التثبيت</a></h2>
    <p>
      يمكنك تجربة دبيان بإقلاع نظام حيّ من خلال قرص CD أو قرص DVD أو مفتاح USB
      دون تثبيت أي ملف على الجهاز. يمكنك أيضا استخدام
      <a href="https://calamares.io">مثبّت كالاماريس</a> المضّمن.
      متوفر لحواسيب 64 بِت فقط. <a href="../CD/live#choose_live">راجع معلومات هذه الطريقة</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Other live ISO</a></li>
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrents</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">شراء أقراص CD أو أقراص DVD أو مفاتيح USB
      من أحد باعة وسائط تثبيت دبيان</a></h2>

   <p>
      العديد من الموزعين يبيعون التوزيعة بأقل من 5 دولارات أمريكية بالإضافة لتكاليف الشحن
      (راجع مواقعهم للتأكد من توفر الشحن الدولي).
   </p>

   <p>هذه بعض الميزات الجوهرية للأقراص:</p>

   <ul>
     <li>يمكنك التثبيت على أجهزة ﻻ تتوفر على اتصال إنترنت.</li>
	 <li>يمكنك تثبيت دبيان دون الحاجة إلى تنزيل كل الحزم.</li>
   </ul>

   <h2><a href="pre-installed">شراء حاسوب مثبّت عليه دبيان</a></h2>
   <p>هناك العديد من الميزات لهذا الخيار:</p>
   <ul>
    <li>ليس عليك تثبيت دبيان.</li>
    <li>التثبيت مضبوط مسبقا ليوافق العتاد.</li>
    <li>يمكن للبائع أن يوفر الدعم التقني.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">استخدم صورة للحوسبة السحابية</a></h2>
    <p><a href="https://cloud.debian.org/images/cloud/"><strong>صورة رسمية للحوسبة السحابية</strong></a>، 
      مبنية من طرف فريق دبيان للحوسبة السحابية، يمكن استخدامها مباشرة على:</p>
    <ul>
      <li>مزوّد OpenStack بصيغة qcow2 أو raw.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="OpenStack image for 64-bit AMD/Intel qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit AMD/Intel raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="OpenStack image for 64-bit ARM qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit ARM raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="OpenStack image for 64-bit Little Endian PowerPC qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit Little Endian PowerPC raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>آلة افتراضية QEMU بصيغة qcow2 أو raw.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="QEMU image for 64-bit AMD/Intel qcow2" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="QEMU image for 64-bit AMD/Intel raw" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="QEMU image for 64-bit ARM qcow2" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="QEMU image for 64-bit ARM raw" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="QEMU image for 64-bit Little Endian PowerPC qcow2" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="QEMU image for 64-bit Little Endian PowerPC raw" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>أمازون EC2 إما على شكل صورة جهاز أو عبر AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machine Images</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>مايكروسوفت Azure على Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
	    <li><a title="Debian 11 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
  </div>
</div>
