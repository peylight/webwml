#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.3</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о третьем обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction apache-log4j1.2 "Исправление проблем безопасности [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] путём удаления поддержки для модулей JMSSink, JDBCAppender, JMSAppender и Apache Chainsaw">
<correction apache-log4j2 "Исправление удалённого выполнения кода [CVE-2021-44832]">
<correction apache2 "Новый выпуск основной ветки разработки; исправление аварийной остановки из-за чтения случайного региона памяти [CVE-2022-22719]; исправление подделки HTTP-запроса [CVE-2022-22720]; исправление записи за пределами выделенного буфера памяти [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Исправление утечки информации [CVE-2021-46671]">
<correction base-files "Обновление для редакции 11.3">
<correction bible-kjv "Исправление ошибки на единицу в поиске">
<correction chrony "Разрешение чтения файла настройки chronyd, создаваемого timemaster(8)">
<correction cinnamon "Исправление аварийной остановки при добавлении онлайн учётной записи с логином">
<correction clamav "Новый стабильный выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2022-20698]">
<correction cups-filters "Apparmor: разрешение чтения из файла настройки cups-browsed для Debian Edu">
<correction dask.distributed "Исправление нежелательного раскрытия списка работающих систем через открытые интерфейсы [CVE-2021-42343]; исправление совместимости с Python 3.9">
<correction debian-installer "Повторная сборка с учётом proposed-updates; обновление ABI ядра Linux до 5.10.0-13">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-ports-archive-keyring "Добавление <q>Debian Ports Archive Automatic Signing Key (2023)</q>; перемещение ключа, использованного в
2021 году, в брелок удалённых ключей">
<correction django-allauth "Исправление поддержки OpenID">
<correction djbdns "Увеличение ограничения размеров данных axfrdns, dnscache и tinydns">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction e2guardian "Исправление отсутствующей проверки SSL-сертификата [CVE-2021-44273]">
<correction epiphany-browser "Временное исправление ошибки в GLib, разрешающее проблему с аварийной остановкой процесса пользовательского интерфейса">
<correction espeak-ng "Удаление ошибочной 50мс задержки при обработке событий">
<correction espeakup "debian/espeakup.service: защита espeakup от системных перегрузок">
<correction fcitx5-chinese-addons "fcitx5-table: добавление отсутствующих зависимостей от fcitx5-module-pinyinhelper и fcitx5-module-punctuation">
<correction flac "Исправление записи за пределами выделенного буфера памяти issue [CVE-2021-0561]">
<correction freerdp2 "Отключение дополнительного журналирования с целью отладки">
<correction galera-3 "Новый выпуск основной ветки">
<correction galera-4 "Новый выпуск основной ветки">
<correction gbonds "Исправление API Treasury для данных о погашении">
<correction glewlwyd "Исправление возможного повышения привилегий">
<correction glibc "Исправление некорректного преобразования из ISO-2022-JP-3 с помощью iconv [CVE-2021-43396]; исправление переполнений буфера [CVE-2022-23218 CVE-2022-23219]; исправление использования указателей после освобождения памяти [CVE-2021-33574]; прекращение замены старых версий файла /etc/nsswitch.conf; упрощение проверки поддерживаемых версий ядра, так как ядра ветки 2.x более не поддерживаются; поддержка установки на ядра с номером выпуска более 255">
<correction glx-alternatives "Установка минимальной альтернативы в переадресованных файлах после изначальной настройки переадресации, чтобы библиотеки не считались отсутствующими до обработки триггеров glx-alternative-mesa">
<correction gnupg2 "scd: исправление CCID-драйвера для SCM SPR332/SPR532; прекращение сетевого взаимодействия в генераторе, которое может приводить к зависаниям">
<correction gnuplot "Исправление деления на ноль [CVE-2021-44917]">
<correction golang-1.15 "Исправление IsOnCurve для значений типа big.Int, которые не являются корректными координатами [CVE-2022-23806]; math/big: предотвращение большого потребления памяти в Rat.SetString [CVE-2022-23772]; cmd/go: предотвращение материализации ветвей в версии [CVE-2022-23773]; исправление исчерпания стека при компиляции сложных вложенных выражений [CVE-2022-24921]">
<correction golang-github-containers-common "Обновление поддержки seccomp для испольования более новых версий ядра">
<correction golang-github-opencontainers-specs "Обновление поддержки seccomp для испольования более новых версий ядра">
<correction gtk+3.0 "Исправление отсутствия результатов поиска при использовании NFS; предотвращение блокировки обработки буфера обмена в Wayland в некоторых пограничных случаях; улучшение печати на принтерах, обнаруженных с помощью mDNS">
<correction heartbeat "Исправление создания /run/heartbeat на системах с systemd">
<correction htmldoc "Исправление чтения за пределами выделенного буфера памяти [CVE-2022-0534]">
<correction installation-guide "Обновление документации и переводов">
<correction intel-microcode "Обновление поставляемого микрокода; уменьшение риска для некоторых проблем безопасности [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "Использование <q>mktemp</q> вместо устаревшего <q>tempfile</q>, что позволяет избежать вывода предупреждений">
<correction lemonldap-ng "Исправление процесса auth в дополнениях для тестирования паролей [CVE-2021-40874]">
<correction libarchive "Исправление извлечения жёстких ссылок в символьные ссылки; исправление обработки списков управления доступом для символьных ссылок [CVE-2021-23177]; запрет на переход по символьным ссылкам при установки флагов файлов [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libgdal-grass "Повторная сборка с учётом 7.8.5-1+deb11u1">
<correction libpod "Обновление поддержки seccomp для испольования более новых версий ядра">
<correction libxml2 "Исправление использования указателей после освобождения памяти [CVE-2022-23308]">
<correction linux "Новый стабильный выпуск основной ветки разработк; [rt] обновление до версии 5.10.106-rt64; увеличение ABI до 13">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработк; [rt] обновление до версии 5.10.106-rt64; увеличение ABI до 13">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработк; [rt] обновление до версии 5.10.106-rt64; увеличение ABI до 13">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработк; [rt] обновление до версии 5.10.106-rt64; увеличение ABI до 13">
<correction mariadb-10.5 "Новый выпуск стабильной ветки разработки; исправления безопасности [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "Добавление Breaks: со старыми версиями libmpich1.0-dev для исправления проблем с обновлениями">
<correction mujs "Исправление переполнения буфера [CVE-2021-45005]">
<correction mutter "Обратный перенос различных исправлений из стабильной ветки основной ветки разработки">
<correction node-cached-path-relative "Исправление загрязнения прототипа [CVE-2021-23518]">
<correction node-fetch "Запрет на перенаправление защищённых заголовков на домены третьих сторон [CVE-2022-0235]">
<correction node-follow-redirects "Запрет передачи заголовков куки между доменами [CVE-2022-0155]; запрет передачи конфиденциальных заголовков между схемами [CVE-2022-0536]">
<correction node-markdown-it "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2022-21670]">
<correction node-nth-check "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2021-3803]">
<correction node-prismjs "Экранирование разметки в выводе командной строки [CVE-2022-23647]; обновление минимизированных файлов с целью гарантировать, что отказ в обслуживании при обработке регулярного выражения исправлен [CVE-2021-3801]">
<correction node-trim-newlines "Исправление отказа в обслуживании при обработке регулярного выражения [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb: отключение неработающей поддержки python, вызывающей ошибки сегментирования; исользование среза openjdk-8-jre (8u312-b07-1)">
<correction nvidia-graphics-drivers-tesla-450 "Новый выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2022-21813 CVE-2022-21814]; nvidia-kernel-support: предоставление /etc/modprobe.d/nvidia-options.conf в качестве шаблона">
<correction nvidia-modprobe "Новый выпуск основной ветки разработки">
<correction openboard "Исправление иконки приложения">
<correction openssl "Новый выпуск основной ветки разработки; исправление аутентификации указателя armv8">
<correction openvswitch "Исправление использования указателей после освобождения памяти [CVE-2021-36980]; исправление установки libofproto">
<correction ostree "Исправление совместимости с eCryptFS; предотвращение бесконечной рекурсии при восстановлении после некоторых ошибок; пометка коммитов как частичных до выполнения их загрузки; исправление ошибки утверждения при использовании обратного переноса или локальной сборки GLib &gt;= 2.71; исправление возможности загружать содержимое OSTree по путям, содержащим символы, которые не могут использоваться в URI (например, символы обратных косых черты) или не входят в ASCII">
<correction pdb2pqr "Исправление совместимости propka с Python 3.8 и более новыми версиями">
<correction php-crypt-gpg "Предотвражение передачи дополнительных опций GPG [CVE-2022-24953]">
<correction php-laravel-framework "Исправление межсайтового скриптинга [CVE-2021-43808], отсутствие блокировки при загрузке исполняемого содержимого [CVE-2021-43617]">
<correction phpliteadmin "Исправление межсайтового скриптинга [CVE-2021-46709]">
<correction prips "Исправление бесконечного оборачивания в случае, если диапазон превышает 255.255.255.255; исправление вывода CIDR с адресами, различающими по первым битам">
<correction pypy3 "Исправление ошибок сборки путём удаления лишних #endif из import.h">
<correction python-django "Исправление отказа в обслуживании [CVE-2021-45115], раскрытия информации [CVE-2021-45116], обхода каталога [CVE-2021-45452]; исправление обратной трассировки при обработке RequestSite/get_current_site() из-за цикличных директив import">
<correction python-pip "Предотвращение состояния гонки при использовании зависимостей, импортированных из zip">
<correction rust-cbindgen "Новый стабильный выпуск основной ветки разработки для поддержки сборки новых версий firefox-esr и thunderbird">
<correction s390-dasd "Прекращение передачи dasdfmt устаревшей опции -f">
<correction schleuder "Переход от булевых значений к целочисленным в случае, если используется адаптер ActiveRecord SQLite3, что восстанавливает работоспособность">
<correction sphinx-bootstrap-theme "Исправление функциональности поиска">
<correction spip "Исправление нескольких межсайтовых скриптингов">
<correction symfony "Исправление CVE-инъекции [CVE-2021-41270]">
<correction systemd "Исправление неуправляемой рекурсии в systemd-tmpfiles [CVE-2021-3997]; понижение статуса зависимости systemd-timesyncd с Depends до Recommends для удаления цикла зависимостей; исправление ошибки монтирования каталога в контейнер с помощью machinectl; исправление регрессии в udev, приводящей к длинным задержкам при обработке разделов, имеющих одну и ту же метку; исправление регрессии при использовании systemd-networkd в непривилегированном LXD-контейнере">
<correction sysvinit "Исправление грамматического разбора вызова <q>shutdown +0</q>; пояснение, что при вызове с <q>time</q> команда shutdown не будет завершена">
<correction tasksel "Установка CUPS для всех *-desktop задач, так как пакета task-print-service больше нет">
<correction usb.ids "Обновление поставляемых данных">
<correction weechat "Исправление отказа в обслуживании [CVE-2021-40516]">
<correction wolfssl "Исправление нескольких ошибок, связанных с обработкой OCSP [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] и поддержкой TLS1.3 [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "Исправление аварийной остановки SIGILL на ЦП без SSE2">
<correction xterm "Исправление переполнения буфера [CVE-2022-24130]">
<correction zziplib "Исправление переполнения буфера [CVE-2020-18442]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction angular-maven-plugin "Более не является полезным">
<correction minify-maven-plugin "Более не является полезным">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
