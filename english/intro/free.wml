#use wml::debian::template title="What does free mean?" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Free as in...?</a></li>
    <li><a href="#licenses">Software Licenses</a></li>
    <li><a href="#choose">How to choose a License?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> In February 1998, a group of people moved to replace the term <a href="https://www.gnu.org/philosophy/free-sw">Free Software</a> with <a href="https://opensource.org/osd">Open Source Software</a>. The terminology debate reflects the underlying philosophical differences, but the practical requirements as well as other things discussed on this site are essentially the same for Free Software and Open Source Software.</p>
</aside>


<h2><a id="freesoftware">Free as in...?</a></h2>

<p>
Many people new to this subject are confused because of the word
"free". It isn't used the way they expect – "free" means "free of cost"
to them. If you look at an English dictionary, it lists almost twenty
different meanings for "free", and only one of them is "at no cost". The
rest refer to "liberty" and "lack of constraint". So, when we speak of
Free Software, we mean freedom, not payment.
</p>

<p>
Software described as free, but only in the sense that you don't need to
pay for it, is hardly free at all, You may be forbidden to pass it on,
and you're most certainly not allowed to modify it. Software licensed at
no cost is usually a weapon in a marketing campaign to promote a related
product or to drive a smaller competitor out of business. There is no
guarantee that it will stay free of charge.
</p>

<p>
To the uninitiated, software is either free or it isn't. Real life
is much more complicated than that, though. To understand what people
imply when they speak about free software, let's take a little detour
and enter the world of software licenses.
</p>

<h2><a id="licenses">Software Licenses</a></h2>

<p>
Copyrights are one method of protecting the creators' rights of certain
types of work. In most countries, newly written software is automatically
copyrighted. A license is the author's way of allowing others to use his
or her creation (software in this case), in ways that are acceptable to
them. It's up to the author to include a license which declares how a
piece of software may be used.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Read more about copyright</a></button></p>

<p>
Of course, different circumstances call for different licenses. Software
companies are looking for ways to protect their assets, so they often
release only compiled code which isn't human readable. They also put many
restrictions on the use of the software. Authors of free software, on
the other hand, are mostly looking for different rules, sometimes even
a combination of the following points:
</p>

<ul>
  <li>It's not allowed to use their code in proprietary software. Since they release their software so that everyone may use it, they don't want to see others steal it. In this case, use of the code is seen as a trust: you may use it, as long as you play by the same rules.</li>
  <li>The authorship's identity must be protected. People take great pride in their work and don't want others to remove their names from the credits or even claim they wrote it themselves.</li>
  <li>Source code must be distributed. One major problem with proprietary software is that you can't fix bugs or customize it, since the source code is not available. Also, a vendor may decide to stop supporting the users' hardware. Distribution of source code, as most free licenses dictate, protects the users by allowing them to customize the software and adjust it to their needs.</li>
  <li>Any work that includes part of the authors' work (also called "derived works" in copyright discussions) must use the same license.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Three of the most widely used free software licenses are the <a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a>, the <a href="https://opensource.org/blog/license/artistic-2-0">Artistic License</a>, and the <a href="https://opensource.org/blog/license/bsd-3-clause">BSD Style License</a>.
</aside>

<h2><a id="choose">How to choose a License?</a></h2>

<p>
Sometimes people write their own licenses, which can be problematic,
so this is frowned upon in the free software community. Too often, the
wording is either ambiguous, or people create conditions that conflict
with each other. Writing a license which holds up in court is even
harder. Luckily, there are a number of Open Source licenses available
to choose from. They have the following things in common:
</p>

<ul>
  <li>Users may install the software on as many machines as they want.</li>
  <li>Any number of people may use the software at one time.</li>
  <li>Users may make as many copies of the software as they want or need and also give these copies to other users (free or open redistribution).</li>
  <li>There are no restrictions on modifying the software (except for keeping certain credits).</li>
  <li>Users may not only distribute the software, they may even sell it.</li>
</ul>

<p>
Especially the last point, which allows people to sell the software,
seems to go against the whole idea of free software, but it's actually
one of its strengths. Since the license allows for free redistribution,
once somebody gets a copy he or she can distribute it as well. People
can even try to sell it.
</p>

<p>
While free software is not completely free of constraints, it
gives users the flexibility to do what they need in order to
get a job done. At the same time, authors' rights are protected
– now, that's freedom.  The Debian project and its members
are strong supporters of free software. We've compiled the <a
href="../social_contract#guidelines">Debian Free Software Guidelines
(DFSG)</a> to come up with a reasonable definition of what constitutes
free software in our opinion. Only software that complies with the DFSG
is allowed in the main Debian distribution.
</p>
