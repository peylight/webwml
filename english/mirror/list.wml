#use wml::debian::template title="Debian Mirrors (worldwide)" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Debian Mirrors per Country</a></li>
  <li><a href="#complete-list">Complete List of Mirrors</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian is distributed (<a href="https://www.debian.org/mirror/">mirrored</a>) on hundreds of servers. If you're planning to <a href="../download">download</a> Debian, try a server nearby first. This will probably be faster and also reduce the load on our central servers.<p>
</aside>

<p class="centerblock">
  Debian mirrors exist in many countries, and for some of them we have
  added a <code>ftp.&lt;country&gt;.debian.org</code> alias. This
  alias usually points to a mirror that syncs regularly and quickly
  and carries all of Debian's architectures.  The Debian archive
  is always available via HTTP at the <code>/debian</code>
  location on the server.
</p>

<p class="centerblock">
  Other mirror sites may have restrictions on what
  they mirror (due to space restrictions).  Just because a site is not the
  country's <code>ftp.&lt;country&gt;.debian.org</code>, does not necessarily
  mean that it is any slower or less up-to-date than the
  <code>ftp.&lt;country&gt;.debian.org</code> mirror.
  In fact, a mirror that carries your architecture and is closer to you as the
  user and, therefore, faster, is almost always preferable to other mirrors
  that are farther away.
</p>

<p>Use the site closest to you for the fastest downloads possible—
whether it is a per-country mirror alias or not.
The program
<a href="https://packages.debian.org/stable/net/netselect-apt">\
<code>netselect-apt</code></a> can be used to
determine the site with the least latency; use a download program such as
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> or
<a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a> for determining the site with the most throughput.
Note that geographic proximity often isn't the most important factor for
determining which machine will serve you best.</p>

<p>
If your system moves around a lot, you may be best served by a "mirror"
that is backed by a global <abbr title="Content Delivery Network">CDN</abbr>.
The Debian project maintains
<code>deb.debian.org</code> for this
purpose, and you can use this in your <code>sources.list</code>—consult
the service's <a href="http://deb.debian.org/">website</a> for details.

<h2 id="per-country">Debian Mirrors per Country</h2>

<table border="0" class="center">
<tr>
  <th>Country</th>
  <th>Site</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Complete List of Mirrors</h2>

<table border="0" class="center">
<tr>
  <th>Host name</th>
  <th>HTTP</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
