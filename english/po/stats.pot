msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../stattrans.pl:282
#: ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr ""

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr ""

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr ""

#: ../../stattrans.pl:290
#: ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr ""

#: ../../stattrans.pl:474
msgid "hits"
msgstr ""

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr ""

#: ../../stattrans.pl:598
#: ../../stattrans.pl:737
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:603
msgid "Translation summary for"
msgstr ""

#: ../../stattrans.pl:606
msgid "Translated"
msgstr ""

#: ../../stattrans.pl:606
#: ../../stattrans.pl:685
#: ../../stattrans.pl:759
#: ../../stattrans.pl:805
#: ../../stattrans.pl:848
msgid "Up to date"
msgstr ""

#: ../../stattrans.pl:606
#: ../../stattrans.pl:760
#: ../../stattrans.pl:806
msgid "Outdated"
msgstr ""

#: ../../stattrans.pl:606
#: ../../stattrans.pl:761
#: ../../stattrans.pl:807
#: ../../stattrans.pl:850
msgid "Not translated"
msgstr ""

#: ../../stattrans.pl:607
#: ../../stattrans.pl:608
#: ../../stattrans.pl:609
#: ../../stattrans.pl:610
msgid "files"
msgstr ""

#: ../../stattrans.pl:613
#: ../../stattrans.pl:614
#: ../../stattrans.pl:615
#: ../../stattrans.pl:616
msgid "bytes"
msgstr ""

#: ../../stattrans.pl:623
msgid "Note: the lists of pages are sorted by popularity. Hover over the page name to see the number of hits."
msgstr ""

#: ../../stattrans.pl:629
msgid "Outdated translations"
msgstr ""

#: ../../stattrans.pl:631
#: ../../stattrans.pl:684
msgid "File"
msgstr ""

#: ../../stattrans.pl:633
msgid "Diff"
msgstr ""

#: ../../stattrans.pl:635
msgid "Comment"
msgstr ""

#: ../../stattrans.pl:636
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:638
msgid "Log"
msgstr ""

#: ../../stattrans.pl:639
msgid "Translation"
msgstr ""

#: ../../stattrans.pl:640
msgid "Maintainer"
msgstr ""

#: ../../stattrans.pl:642
msgid "Status"
msgstr ""

#: ../../stattrans.pl:643
msgid "Translator"
msgstr ""

#: ../../stattrans.pl:644
msgid "Date"
msgstr ""

#: ../../stattrans.pl:651
msgid "General pages not translated"
msgstr ""

#: ../../stattrans.pl:652
msgid "Untranslated general pages"
msgstr ""

#: ../../stattrans.pl:657
msgid "News items not translated (low priority)"
msgstr ""

#: ../../stattrans.pl:658
msgid "Untranslated news items"
msgstr ""

#: ../../stattrans.pl:663
msgid "Consultant/user pages not translated (low priority)"
msgstr ""

#: ../../stattrans.pl:664
msgid "Untranslated consultant/user pages"
msgstr ""

#: ../../stattrans.pl:669
msgid "International pages not translated (very low priority)"
msgstr ""

#: ../../stattrans.pl:670
msgid "Untranslated international pages"
msgstr ""

#: ../../stattrans.pl:675
msgid "Translated pages (up-to-date)"
msgstr ""

#: ../../stattrans.pl:682
#: ../../stattrans.pl:832
msgid "Translated templates (PO files)"
msgstr ""

#: ../../stattrans.pl:683
#: ../../stattrans.pl:835
msgid "PO Translation Statistics"
msgstr ""

#: ../../stattrans.pl:686
#: ../../stattrans.pl:849
msgid "Fuzzy"
msgstr ""

#: ../../stattrans.pl:687
msgid "Untranslated"
msgstr ""

#: ../../stattrans.pl:688
msgid "Total"
msgstr ""

#: ../../stattrans.pl:705
msgid "Total:"
msgstr ""

#: ../../stattrans.pl:739
msgid "Translated web pages"
msgstr ""

#: ../../stattrans.pl:742
msgid "Translation Statistics by Page Count"
msgstr ""

#: ../../stattrans.pl:757
#: ../../stattrans.pl:803
#: ../../stattrans.pl:847
msgid "Language"
msgstr ""

#: ../../stattrans.pl:758
#: ../../stattrans.pl:804
msgid "Translations"
msgstr ""

#: ../../stattrans.pl:785
msgid "Translated web pages (by size)"
msgstr ""

#: ../../stattrans.pl:788
msgid "Translation Statistics by Page Size"
msgstr ""

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr ""

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr ""

