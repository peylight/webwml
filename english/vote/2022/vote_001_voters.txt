-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4           alexm	Alex Muntada
    5           alexp	Alex Pennace
    6        amacater	Andrew Martin Adrian Cater
    7           amaya	Amaya Rodrigo Sastre
    8             ana	Ana Beatriz Guerrero López
    9         anarcat	Antoine Beaupré
   10            anbe	Andreas Beckmann
   11            andi	Andreas B. Mundt
   12           angel	Angel Abad
   13          ansgar	Ansgar
   14             apo	Markus Koschany
   15         aurel32	Aurelien Jarno
   16              az	Alexander Zangerl
   17        azekulic	Alen Zekulic
   18      babelouest	Nicolas Mora
   19        ballombe	Bill Allombert
   20             bam	Brian May
   21             bap	Barak A. Pearlmutter
   22           bartm	Bart Martens
   23             bas	Bas Zoetekouw
   24         bblough	William Blough
   25           bdale	Bdale Garbee
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26          bengen	Hilko Bengen
   27          bernat	Vincent Bernat
   28         bgoglin	Brice Goglin
   29           biebl	Michael Biebl
   30           blade	Eduard Bloch
   31           bluca	Luca Boccassi
   32             bod	Brendan O'Dea
   33           bootc	Chris Boot
   34          boutil	Cédric Boutillier
   35         bpellin	Brian Pellin
   36         bremner	David Bremner
   37        calculus	Jerome Georges Benoit
   38          carnil	Salvatore Bonaccorso
   39           cavok	Domenico Andreoli
   40          chrism	Christoph Martin
   41       chronitis	Gordon Ball
   42             ckk	Christian Kastner
   43           clint	Clint Adams
   44        codehelp	Neil Williams
   45           cwryu	Changwoo Ryu
   46          czchen	ChangZhuo Chen
   47           dapal	David Paleino
   48        deltaone	Patrick Franz
   49       dktrkranz	Luca Falavigna
   50          dlange	Daniel Lange
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51           dlehn	David I. Lehn
   52             dmn	Damyan Ivanov
   53             dod	Dominique Dumont
   54             dom	Dominic Hargreaves
   55             don	Don Armstrong
   56         donkult	David Kalnischkies
   57       dtorrance	Douglas Andrew Torrance
   58          eevans	Eric Evans
   59        ehashman	Elana Hashman
   60          elbrus	Paul Mathijs Gevers
   61             ema	Emanuele Rocca
   62        emollier	Étienne Mollier
   63          enrico	Enrico Zini
   64        eriberto	Joao Eriberto Mota Filho
   65            eric	Eric Dorland
   66           eriks	Erik Schanze
   67             evo	Davide Puricelli
   68        federico	Federico Ceratto
   69           felix	Félix Sipma
   70          fgeyer	Felix Geyer
   71         florian	Florian Ernst
   72         fpeters	Frederic Peters
   73        francois	Francois Marier
   74         gabriel	Gabriel F. T. Gomes
   75           georg	Georg Faerber
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76        georgesk	Georges Khaznadar
   77             gio	Giovanni Mascellani
   78         giovani	Giovani Augusto Ferreira
   79           gladk	Anton Gladky
   80          glondu	Stéphane Glondu
   81          gniibe	NIIBE Yutaka
   82         godisch	Martin A. Godisch
   83          gregoa	Gregor Herrmann
   84            gspr	Gard Spreemann
   85         guilhem	Guilhem Moulin
   86         guillem	Guillem Jover
   87          gusnan	Andreas Rönnquist
   88           gwolf	Gunnar Wolf
   89            haas	Christoph Haas
   90        hartmans	Sam Hartman
   91           hefee	Sandro Knauß
   92         hertzog	Raphaël Hertzog
   93      hlieberman	Harlan Lieberman-Berg
   94         hoexter	Sven Hoexter
   95          holger	Holger Levsen
   96      hvhaugwitz	Hannes von Haugwitz
   97             ijc	Ian James Campbell
   98       intrigeri	Intrigeri
   99         jaldhar	Jaldhar H. Vyas
  100        jamessan	James McCoy
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101           jandd	Jan Dittberner
  102          jaqque	John Robinson
  103            jcfp	Jeroen Ploemen
  104             jdg	Julian Gilbey
  105          jlines	John Lines
  106           joerg	Joerg Jaspert
  107          johfel	Johann Felix Soden
  108         joostvb	Joost van Baal
  109           jordi	Jordi Mallach
  110           josue	Josué Ortega
  111         joussen	Mario Joussen
  112       jpmengual	Jean-Philippe MENGUAL
  113          jpuydt	Julien Puydt
  114        jredrejo	José L. Redrejo Rodríguez
  115        jspricke	Jochen Sprickerhof
  116          kartik	Kartik Mistry
  117          kenhys	HAYASHI Kentaro
  118            kibi	Cyril Brulebois
  119            knok	Takatsugu Nokubi
  120           kobla	Ondřej Kobližek
  121           krala	Antonin Kral
  122      kritzefitz	Sven Bartscher
  123            kula	Marcin Kulisz
  124         larjona	Laura Arjona Reina
  125        lavamind	Jerome Charaoui
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126      lazyfrosch	Markus Frosch
  127         ldrolez	Ludovic Drolez
  128         lechner	Felix Lechner
  129         lenharo	Daniel Lenharo de Souza
  130             leo	Carsten Leonhardt
  131        lfaraone	Luke Faraone
  132         lingnau	Anselm Lingnau
  133          lkajan	Laszlo Kajan
  134           lucab	Luca Bruno
  135           lucas	Lucas Nussbaum
  136         lyknode	Baptiste Beauplat
  137         madduck	Martin F. Krafft
  138           mattb	Matthew Brown
  139          mattia	Mattia Rizzolo
  140            maxx	Martin Wuertele
  141            maxy	Maximiliano Curia
  142         mbehrle	Mathias Behrle
  143              md	Marco d'Itri
  144            mejo	Jonas Meurer
  145          merker	Karsten Merker
  146           micha	Micha Lenk
  147            mika	Michael Prokop
  148           milan	Milan Kupcevic
  149        mjeanson	Michael Jeanson
  150           moray	Moray Allan
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151           mpitt	Martin Pitt
  152        mquinson	Martin Quinson
  153     mtecknology	Michael Lustfield
  154           murat	Murat Demirten
  155            myon	Christoph Berg
  156          nbreen	Nicholas Breen
  157           neilm	Neil McGovern
  158           nickm	Nick Morrott
  159         nicolas	Nicolas Boulenguez
  160          nilesh	Nilesh Patra
  161            noel	Noèl Köthe
  162         noodles	Jonathan McDowell
  163        nthykier	Niels Thykier
  164           ntyni	Niko Tyni
  165           olasd	Nicolas Dandrimont
  166         olebole	Ole Streicher
  167            olly	Olly Betts
  168           osamu	Osamu Aoki
  169            pabs	Paul Wise
  170    paddatrapper	Kyle Robbertze
  171          paride	Paride Legovini
  172             peb	Pierre-Elliott Bécue
  173             pgt	Pierre Gruet
  174           philh	Philip Hands
  175             pik	Paul Cannon
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176            pini	Gilles Filippini
  177             pjb	Phil Brooke
  178          plessy	Charles Plessy
  179       pmatthaei	Patrick Matthäi
  180           pollo	Louis-Philippe Véronneau
  181          pollux	Pierre Chifflier
  182        pvaneynd	Peter Van Eynde
  183             ras	Russell Stuart
  184         rbalint	Balint Reczey
  185          rbasak	Robie Basak
  186         reichel	Joachim Reichel
  187      rfrancoise	Romain Francoise
  188          rhonda	Rhonda D'Vine
  189         rlaager	Richard Laager
  190        rmayorga	Rene Mayorga
  191            roam	Peter Pentchev
  192         roberto	Roberto C. Sanchez
  193        roehling	Timo Röhling
  194             ron	Ron Lee
  195        rousseau	Ludovic Rousseau
  196             rra	Russ Allbery
  197     rvandegrift	Ross Vandegrift
  198        santiago	Santiago Ruano Rincón
  199           satta	Sascha Steinbiss
  200             seb	Sebastien Delafond
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201       sebastien	Sébastien Villemot
  202         serpent	Tomasz Rybak
  203           sesse	Steinar H. Gunderson
  204        siretart	Reinhard Tartler
  205             sjr	Simon Richter
  206           skitt	Stephen Kitt
  207            smcv	Simon McVittie
  208             smr	Steven Michael Robbins
  209           smurf	Matthias Urlichs
  210       spwhitton	Sean Whitton
  211       sramacher	Sebastian Ramacher
  212        stefanor	Stefano Rivera
  213  stephanlachnit	Stephan Lachnit
  214           steve	Steve Kostecke
  215       sthibault	Samuel Thibault
  216             sto	Sergio Talens-Oliag
  217          stuart	Stuart Prescott
  218           szlin	SZ Lin
  219            tach	Taku Yasui
  220          taffit	David Prévot
  221          takaki	Takaki Taniguchi
  222           taowa	
  223             tbm	Martin Michlmayr
  224        terceiro	Antonio Terceiro
  225              tg	Thorsten Glaser
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226         thibaut	Thibaut Jean-Claude Paumard
  227           thijs	Thijs Kinkhorst
  228           tiago	Tiago Bortoletto Vaz
  229          tianon	Tianon Gravi
  230            tiwe	Timo Weingärtner
  231        tjhukkan	Teemu Hukkanen
  232        tmancill	Tony Mancill
  233           toddy	Tobias Quathamer
  234            toni	Toni Mueller
  235         treinen	Ralf Treinen
  236           troyh	Troy Heber
  237        tvainika	Tommi Vainikainen
  238         tzafrir	Tzafrir Cohen
  239            ucko	Aaron M. Ucko
  240       ultrotter	Guido Trotter
  241         unit193	Unit 193  
  242           urbec	Judit Foglszinger
  243         vagrant	Vagrant Cascadian
  244         vasudev	Vasudev Sathish Kamath
  245            vivi	Vincent Prat
  246          vvidic	Valentin Vidic
  247          wagner	Hanno Wagner
  248        weinholt	Göran Weinholt
  249          wijnen	Bas Wijnen
  250          wookey	Wookey
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251          wouter	Wouter Verhelst
  252            wrar	Andrey Rahmatullin
  253          xluthi	Xavier Lüthi
  254            zack	Stefano Zacchiroli
  255            zeha	Christian Hofstaedtler
  256            zhsj	Shengjing Zhu
  257            zigo	Thomas Goirand
  258       zugschlus	Marc Haber
