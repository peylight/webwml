#use wml::debian::translation-check translation="711f7fdbc4c6ba345553c2acb65646f482ec657c"
           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Representação gráfica dos resultados">
               </a>
           </p>
             <p>
               No gráfico acima, quaisquer nós de cor rosa implicam que
               a opção não superou a maioria, o azul é o
               vencedor. O octógono é usado para as opções que não superaram o
               padrão.
           </p>
           <ul>
<li>Opção 1 "Gergely Nagy"</li>
<li>Opção 2 "Moray Allan"</li>
<li>Opção 3 "Lucas Nussbaum"</li>
<li>Opção 4 "None Of The Above (nenhuma das anteriores)"</li>
           </ul>
            <p>
               Na tabela a seguir, o par[linha x][coluna y] representa
               os votos que a opção x recebeu sobre a opção y. Uma
               <a href="https://en.wikipedia.org/wiki/Schwartz_method">explicação
               mais detalhada da matriz de duelos</a> pode ajudar na
               compreensão da tabela. Para entender o método de Condorcet, a
               <a href="https://en.wikipedia.org/wiki/Condorcet_method">página
               da Wikipedia</a> é bastante informativa.
           </p>
           <table class="vote">
             <caption class="center"><strong>A matriz de duelos</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="4" class="center">Opções</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
                   <th>    4 </th>
              </tr>
                 <tr>
                   <th>Opção 1  </th>
                   <td>&nbsp;</td>
                   <td>   88 </td>
                   <td>   62 </td>
                   <td>  319 </td>
                 </tr>
                 <tr>
                   <th>Opção 2  </th>
                   <td>  252 </td>
                   <td>&nbsp;</td>
                   <td>  141 </td>
                   <td>  355 </td>
                 </tr>
                 <tr>
                   <th>Opção 3  </th>
                   <td>  279 </td>
                   <td>  210 </td>
                   <td>&nbsp;</td>
                   <td>  353 </td>
                 </tr>
                 <tr>
                   <th>Opção 4  </th>
                   <td>   51 </td>
                   <td>   24 </td>
                   <td>   30 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

Olhando a linha 2, coluna 1, Moray Allan<br/>
recebeu 252 votos sobre Gergely Nagy<br/>
<br/>
Olhando a linha 1, coluna 2, Gergely Nagy<br/>
recebeu 88 votos sobre Moray Allan.<br/>
              <h3>Pair-wise defeats</h3>
              <ul>
                <li>A opção 2 vence a opção 1 por ( 252 -   88) =  164 votos.</li>
                <li>A opção 3 vence a opção 1 por ( 279 -   62) =  217 votos.</li>
                <li>A opção 1 vence a opção 4 por ( 319 -   51) =  268 votos.</li>
                <li>A opção 3 vence a opção 2 por ( 210 -  141) =   69 votos.</li>
                <li>A opção 2 vence a opção 4 por ( 355 -   24) =  331 votos.</li>
                <li>A opção 3 vence a opção 4 por ( 353 -   30) =  323 votos.</li>
              </ul>
              <h3>O conjunto de Schwartz contém</h3>
              <ul>
                <li>A opção 3 "Lucas Nussbaum"</li>
              </ul>
              <h3>Os(As) vencedores(as)</h3>
              <ul>
                <li>A opção 3 "Lucas Nussbaum"</li>
              </ul>
              <p>
               O Debian usa o método de Condorcet para votação.
               De forma simplista, o método de Condorcet puro
               pode ser declarado da seguinte forma:<br/>
               <q>Considere todos os possíveis enfrentamentos entre pares de
                  candidatos(as).
                  O(A) vencedor(a) do Condorcet, se houver, é o(a)
                  candidato(a) que vencer cada um dos(as) outros(as)
                  candidatos(as) nesse enfrentamento por pares.</q>
               O problema é que, em eleições complexas, pode muito bem haver
               uma relação circular em que A vence B, B vence C,
               e C vence A. A maioria das variações no Condorcet usa vários
               meios de resolver o empate. Veja
               <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">Cloneproof Schwartz Sequential Dropping (o método Schulze)</a>
               para mais detalhes. A variante do Debian é explicada na
               <a href="$(HOME)/devel/constitution">constituição</a>,
               especificamente, A.6.
              </p>
