#use wml::debian::template title="Porte para PowerPC (PowerMac)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="b5617866ae4047b55b0439a5c5796df65590cec3"

<h1 class="center">Página do Debian GNU/Linux PowerPC PowerMac</h1>

<p>
Aqui estão alguns destaques da instalação do Debian no PowerMac. Para
instruções detalhadas, por favor estude
<a href="$(HOME)/releases/stable/powerpc/">o bom manual de instalação</a>.
O time do instalador do Debian tem gasto muitas horas tentando responder
antecipadamente aos questionamentos e têm oferecido guias escritos
excelentes para você instalar o Debian
</p>

<p>
É certamente possível, e realmente existem várias soluções elegantes
para os Macs NewWorld, fazer dual boot entre seu sistema Debian
PowerMac com o Mac OS e/ou Mac OS X. Mas se você está planejando fazer uma
nova instalação do Mac OS X, faça antes de instalar o Debian. O instalador
do Mac OS X não é muito legal com sistemas existentes. De imediato,
você também tem a opção de executar o Debian dentro de um sistema
Darwin.
</p>

<p>
A instalação do Debian usa uma série de kernel 2.6. Este kernel deve
suportar a maior parte do hardware PowerMac. A série 2.4 de kernel para
powerpc está disponível para usuários(as) com módulos externamente
fornecidos que não foram portados para kernels 2.6. Entretanto, o uso
deste kernel é fortemente desencorajado.
</p>

<p>
Você precisará particionar seu disco; o Linux deve ser instalado em
suas próprias partições. Se você tiver um sistema de disco único, isto
vai implicar na cópia de segurança de tudo em seu sistema e a recuperação
dos dados após o particionamento estiver completo. Algumas ferramentas
de particionamento de terceiros podem ser capazes de 'encolher' uma
partição, e assim você terá mais espaço para mais partições no seu
disco sem destruir o que já está lá. Mas elas com certeza vão sugerir
uma cópia de segurança antes. O Drive Setup não oferece essa opção,
ele apaga o drive inteiro.
</p>

<p>
1 GB provavelmente é espaço suficiente para um sistema Linux experimental.
Você pode se virar com menos, talvez tão pouco quanto 400&nbsp;MB para um
sistema realmente básico, mas você certamente vai querer mais que somente
o básico.
</p>

<p>
Após particionar seu disco, você vai precisar obter um CD de instalação
ou fazer o download do sistema instalador. Uma vez que finalmente você
pare para fazer a instalação (preferencialmente com o manual de
instalação em mãos), você provavelmente vai gastar 2 ou 3 horas
fazendo tudo acontecer. Alguém experiente em instalações pode conseguir
uma instalação básica em menos de meia hora.
</p>

<p>
Se você realmente fica desconfortável com linhas de comando, então instale
o desktop X após ter terminado a instalação do sistema básico.
Mas aproveite para repensar isso: existe um mundo de potencialidades na
linha de comando do Linux. Algumas coisas que são muito difíceis de fazer
em uma interface gráfica, são muito rápidas e eficientes na linha de
comando. E também, já que o sistema Linux é baseado em linha de comando,
existem algumas funções que somente são acessíveis pela linha de comando.
O sistema padrão define 6 consoles de linha de comando, e nenhum gráfico.
Você pode fazer muita coisa nesses consoles enquanto você navega pela web...
e desse modo você pode ter o melhor dos dois mundos.
</p>

<p>
A plataforma PowerPC roda muito bem com Linux. É muito
respeitada no meio do mundo Linux. Curta e lembre-se de
contribuir de volta!
</p>

<p>
Para ajuda com o Quik em Macs OldWorld, veja
<a href="http://penguinppc.org/bootloaders/quik/">\
http://penguinppc.org/bootloaders/quik/</a>
</p>

<p>
Para informações detalhadas sobre os vários modelos Mac e, em particular,
informações sobre trabalhar com OpenFirmware com qualquer modelo,
verifique a <a href="http://www.netbsd.org/Ports/macppc/models.html">lista
NetBSD para modelos PowerPC</a>. Como essa instalação requer que o
OpenFirmware seja configurado antes, eles(as) são especialistas neste
assunto.
</p>
