
<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>
primary source for all security related information, search options</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON list</a>
  contains CVE description, package name, Debian bug number, package versions with fix, no DSA included
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA list</a>
  contains DSA including date, related CVE's numbers, package versions with fix
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA list</a>
  contains DLA including date, related CVE's numbers, package versions with fix
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
DSA announcements</a> (Debian Security Advisories)</li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
DLA announcements</a> (Debian Security Advisories of Debian LTS)</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa">RSS</a>
of the DSA or <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa-long">RSS</a> long version including the text of the advisory</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla">RSS</a>
of the DLA or <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla-long">RSS</a> long version including the text of the advisory</li>

<li><a href="oval">Oval files</a></li>

<li>Lookup a DSA (uppercase is important)<br>
e.g. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Lookup a DLA ( -1 is important)<br>
e.g. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Lookup a CVE<br>
e.g. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
