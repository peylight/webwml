#use wml::debian::template title="Histórico da constituição do Debian v 1.7" BARETITLE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="7de52c11c6e319c053f658745650d45aeddbb0d3"

<h1>Histórico das constituições do Projeto Debian (v1.7)</h1>

<p>Versão 1.7 ratificada em 14 de agosto de 2016.</p>

<p>Substituída
pela <a href="constitution.1.8">versão 1.8</a> ratificada em 28 de janeiro de 2022,
e pela <a href="constitution">versão atual 1.9</a> ratificada em 26 de março de 2022.</p>

<p>Substitui
a   <a href="constitution.1.6">versão 1.6</a> ratificada em 13 de dezembro de 2015,
a   <a href="constitution.1.5">versão 1.5</a> ratificada em 9 de janeiro de 2015,
a   <a href="constitution.1.4">versão 1.4</a> ratificada em 7 de outubro de 2007,
a   <a href="constitution.1.3">versão 1.3</a> ratificada em 24 de setembro de 2006,
a   <a href="constitution.1.2">versão 1.2</a> ratificada em 29 de outubro de 2003,
a   <a href="constitution.1.1">versão 1.1</a> ratificada em 21 de junho de 2003,
e a <a href="constitution.1.0">versão 1.0</a> ratificada em 2 de dezembro de 1998.</p>

<toc-display />

<toc-add-entry name="item-1">1. Introdução</toc-add-entry>

<p><cite>O Projeto Debian é uma associação de indivíduos que têm
a causa comum de criar um sistema operacional livre.</cite></p>

<p>Este documento descreve a estrutura organizacional para tomadas
de decisões formais no Projeto. Ele não descreve os objetivos do
Projeto ou como alcançá-los, ou contém quaisquer políticas exceto
aquelas diretamente relacionadas ao processo de tomada de decisões.</p>

<toc-add-entry name="item-2">2. Grupos e indivíduos na tomada de decisões
</toc-add-entry>

<p>Cada decisão no Projeto é tomada por um ou mais dos seguintes:</p>

<ol>
  <li>Os(As) desenvolvedores(as), por via de Resolução Geral ou uma eleição;
  </li>

  <li>O(A) líder do Projeto;</li>

  <li>O comitê técnico e/ou seu(sua) presidente;</li>

  <li>O(A) desenvolvedor(a) individual trabalhando em uma tarefa particular;
  </li>

  <li>Delegados(as) apontados(as) pelo(a) líder do Projeto para tarefas
  específicas;</li>

  <li>O(A) secretário(a) do Projeto.</li>
</ol>

<p>A maior parte do restante deste documento descreverá os poderes destes
grupos, sua composição e nomeação e o procedimento para suas tomadas de
decisões. Os poderes de uma pessoa ou grupo podem estar sujeitos a revisão
e/ou limitação por outros; neste caso o grupo revisor ou a pessoa encarregada
indicará isso. <cite>Na lista acima, uma pessoa ou grupo é normalmente
listada(o) antes de quaisquer pessoas ou grupos cujas decisões ela(e) pode
anular ou aqueles que ela(e) (ajuda a) nomear - mas nem todos os listados
anteriormente podem anular todos os listados posteriormente.</cite></p>

<h3>2.1. Regras Gerais</h3>

<ol>
  <li>
    <p>Nada nesta constituição impõe uma obrigação a alguém de trabalhar
    para o Projeto. Uma pessoa que não quer fazer uma tarefa
    que foi delegada ou atribuída a ela, não precisa fazê-la.
    No entanto, ela não pode trabalhar ativamente contra estas regras
    e decisões genuinamente tomadas sob ela.</p>
  </li>

  <li>
    <p>Uma pessoa pode ter vários cargos, exceto que o(a) líder do Projeto,
    o(a) secretário(a) do Projeto e o(a) presidente do comitê técnico devem ser
    distintos, e que o(a) líder não pode se nomear como seu próprio
    Delegado(a).</p>
  </li>

  <li>
    <p>Uma pessoa pode deixar o Projeto ou renunciar de um cargo em particular
    que ela tenha, a qualquer momento, fazendo isso publicamente.</p>
  </li>
</ol>

<toc-add-entry name="item-3">3. desenvolvedores(as) Individuais</toc-add-entry>

<h3>3.1. Poderes</h3>

<p>Um(a) desenvolvedor(a) individual pode</p>

<ol>
  <li>tomar qualquer decisão técnica ou não técnica no que diz respeito
  ao seu próprio trabalho;</li>

  <li>propor ou apadrinhar rascunhos de Resoluções Gerais;</li>

  <li>propor a si mesmo como um(a) candidato(a) a líder do Projeto nas
  eleições;</li>

  <li>votar em Resoluções Gerais e em eleições para líder.</li>
</ol>

<h3>3.2. Composição e nomeação</h3>

<ol>
  <li>
    <p>Os(As) desenvolvedores(as) são voluntários(as) que concordam em promover
    os objetivos do Projeto na medida em que participam dele, e
    que mantêm pacote(s) para o Projeto ou fazem outros trabalhos que
    o(a)(s) Delegado(a)(s) do(a) líder do Projeto considere(m) que vale a pena.</p>
  </li>

  <li>
    <p>O(A)(s) Delegado(a)(s) do(a) líder do Projeto pode(m) escolher não
    admitir novos(as) desenvolvedores(as), ou expulsar desenvolvedores(as)
    existentes. <cite>Se os(as) desenvolvedores(as) acham que os(as)
    Delegados(as) estão abusando de sua autoridade eles(as) podem, é claro,
    anular a decisão por meio de uma Resolução Geral - veja &sect;4.1(3),
    &sect;4.2.</cite></p>
  </li>
</ol>

<h3>3.3. Procedimento</h3>

<p>Os(As) desenvolvedores(as) podem tomar decisões quando eles(as) acharem
conveniente.</p>

<toc-add-entry name="item-4">4. Os(As) desenvolvedores(as) por meio de uma
Resolução Geral ou eleição</toc-add-entry>


<h3>4.1. Poderes</h3>

<p>Juntos, os(as) desenvolvedores(as) podem:</p>

<ol>
  <li>
    <p>Nomear ou destituir o(a) líder do Projeto.</p>
  </li>

  <li>
    <p>Emendar esta constituição, desde que concordem em uma
    maioria de 3:1.</p>
  </li>

  <li>
    <p>Tomar ou anular qualquer decisão legitimada pelos poderes do(a) líder do
    Projeto ou por um(a) Delegado(a).</p>
  </li>

  <li>
    <p>Tomar ou anular qualquer decisão legitimada pelos poderes do Comitê
    Técnico, desde que concordem em uma maioria de 2:1.</p>
  </li>

  <li>
    <p>Criar, substituir e retirar declarações e documentos de políticas
       não técnicas.</p>

    <p>Estes incluem documentos que descrevem os objetivos do projeto, seu
       relacionamento com outras entidades de software livre e políticas
       não técnicas, como os termos de licença de software livre que o software
       no Debian deve atender.</p>

    <p>Eles podem também incluir declarações de posicionamento sobre assuntos
    do dia.</p>

    <ol style="list-style: decimal;">
      <li>Um Documento Fundamental é um documento ou declaração considerada
       crítica para a missão e os propósitos do Projeto.</li>
      <li>Os Documentos Fundamentais são os trabalhos intitulados <q>Debian
       Social Contract</q> (<q>Contrato Social Debian</q>) e <q>Debian Free
       Software Guidelines</q> (<q>Definição Debian de Software
       Livre</q>).</li>
      <li>Um Documento Fundamental requer uma maioria 3:1 para sua
       substituição. Novos Documentos Fundamentais são emitidos e os
       existentes são retirados, através de emendas à lista de Documentos
       Fundamentais nesta constituição.</li>
    </ol>

  </li>

  <li>
    <p>Tomar decisões sobre propriedades guardadas em confiança para
    propósitos relacionados ao Debian. (Veja &sect;9.).</p>
  </li>

  <li>
    <p>Em caso de um desentendimento entre o(a) líder do Projeto e o(a)
    secretário(a) incumbido, nomear um(a) novo(a) secretário(a).</p>
  </li>
</ol>

<h3>4.2. Procedimento</h3>

<ol>
  <li>
    <p>Os(As) desenvolvedores(as) seguem o Procedimento de Resolução Padrão,
    abaixo. Uma resolução ou emenda é introduzida se proposta por qualquer
    desenvolvedor(a) e apadrinhada por pelo menos K outros(as)
    desenvolvedores(as), ou se proposta pelo(a) líder do Projeto ou pelo
    comitê técnico.</p>
  </li>

  <li>
    <p>Adiando uma decisão tomada pelo(a) líder do Projeto ou seu(sua)
    Delegado(a):</p>

    <ol>
      <li>Se o(a) líder do Projeto ou seu(sua) Delegado(a), ou o comitê técnico,
      tomou uma decisão, então os(as) desenvolvedores(as) podem anulá-la
      passando uma resolução para tal; veja &sect;4.1(3).</li>

      <li>Se tal resolução for apadrinhada por pelo menos 2K
      desenvolvedores(as), ou se for proposta pelo comitê técnico, a resolução
      coloca a decisão imediatamente em espera (desde que a resolução diga
      isso).</li>

      <li>Se a decisão original foi para mudar um período de discussão ou
      um período de votação, ou a resolução é para anular o comitê técnico,
      então apenas K desenvolvedores(as) precisam apadrinhar a resolução para
      que seja possível colocar a decisão imediatamente em espera.</li>

      <li>Se a decisão é colocada em espera, uma votação imediata acontece
      para determinar se a decisão deve continuar até que a votação completa
      sobre a decisão seja feita ou se a implementação da decisão original
      será adiada até lá. Não há quorum para este procedimento de votação
      imediata.</li>

      <li>Se o(a) líder do Projeto (ou o(a) Delegado(a)) retira a decisão
      original, a votação torna-se irrelevante, e não é mais
      conduzida.</li>
    </ol>
  </li>

  <li>
    <p>
       Os votos são recebidos pelo(a) secretário(a) do Projeto. os votos, as
       tabulações e os resultados não são revelados durante o período de
       votação; depois da votação o(a) secretário(a) do Projeto lista todos os
       votos. O período de votação é de 2 semanas mas pode ser variado em até 1
       semana pelo(a) líder do Projeto.
    </p>
  </li>

  <li>
    <p>O período de discussão mínimo é de 2 semanas mas pode ser variado
    em até 1 semana pelo(a) líder do Projeto. O(A) líder do Projeto tem um
    voto de minerva. Há um quorum de 3Q.</p>
  </li>

  <li>
    <p>As propostas, padrinhos/madrinhas, emendas, chamadas para votação e
    outras ações formais são feitas através de anúncios em uma lista
    de e-mails publicamente legível designada pelo(a)(s) Delegado(a)(s)
    do(a) líder do Projeto; qualquer desenvolvedor(a) poderá postar lá.</p>
  </li>

  <li>
    <p>Os votos são enviados por e-mail em uma maneira que convenha ao(a)
    secretário(a). O(A) secretário(a) determina para cada votação se os
    votantes podem alterar seus votos ou não.</p>
  </li>

  <li>
    <p>Q é a metade da raiz quadrada do número atual de desenvolvedores(as).
    K é Q ou 5, o que for menor. Q e K não precisam ser inteiros e não
    são arredondados.</p>
  </li>
</ol>

<toc-add-entry name="item-5">5. Líder do Projeto</toc-add-entry>

<h3>5.1. Poderes</h3>

<p>O(A) <a href="leader">Líder do Projeto</a> pode:</p>

<ol>
  <li>
    <p>Nomear Delegados(as) ou delegar decisões ao comitê técnico.</p>

    <p>O(A) líder pode definir uma área de responsabilidade ou uma
    decisão específica e passá-la para outro(a) desenvolvedor(a) ou
    para o comitê técnico.</p>

    <p>Uma vez que uma decisão particular tenha sido delegada e tomada, o(a)
    líder do Projeto não pode voltar atrás na delegação; no entanto,
    ele(a) pode voltar atrás em uma delegação corrente de uma área de
    responsabilidade em particular.</p>
  </li>

  <li>
    <p>Emprestar autoridade a outros(as) desenvolvedores(as).</p>

    <p>O(A) líder do Projeto pode criar declarações de suporte para pontos
    de vista ou para outros membros do projeto, quando requisitado
    ou não; estas declarações têm força se, e apenas se, o(a) líder
    receber poderes para tomar a decisão em questão.</p>
  </li>

  <li>
    <p>Tomar qualquer decisão que requeira ação urgente.</p>

    <p>Isto não se aplica a decisões que tornaram-se gradualmente
    urgentes pela falta de ação relevante, a menos que haja um
    prazo limite fixado.</p>
  </li>

  <li>
    <p>Tomar qualquer decisão para a qual ninguém mais tem responsabilidade.</p>
  </li>

  <li>
    <p>Propor rascunhos de Resoluções Gerais e emendas.</p>
  </li>

  <li>
    <p>Juntamente com o comitê técnico, nomear novos membros para o
    Comitê. (Veja &sect;6.2.)</p>
  </li>

  <li>
    <p>Usar um voto de minerva quando os(as) desenvolvedores(as) votam.</p>

    <p>O(A) líder do Projeto tem também um voto normal em tais votações.</p>
  </li>

  <li>
    <p>Variar o período de discussão para votações dos(as) desenvolvedores(as)
    (como acima).</p>
  </li>

  <li>
    <p>Liderar discussões entre os(as) desenvolvedores(as).</p>

    <p>O(A) líder do Projeto deveria tentar participar em discussões entre
    os(as) desenvolvedores(as) de uma maneira útil que procure fazer com que a
    discussão toque nos pontos chaves do momento. O(A) líder do Projeto não
    deverá usar sua posição de liderança para promover seus próprios
    pontos de vista.</p>
  </li>

  <li>
    <p>Em consulta com os(as) desenvolvedores(as), tomar decisões que afetem as
    propriedades mantidas em confiança para propósitos relacionados ao
    Debian. (Veja &sect;9.). Tais decisões são comunicadas aos membros pelo(a)
    líder do Projeto ou seu(sua)(s) Delegado(a)(s). Gastos maiores deveriam
    ser propostos e debatidos na lista de discussão antes dos fundos
    serem despendidos.</p>
  </li>
  <li>
    <p>Adicionar ou remover organizações da lista de organizações
    de confiança (veja &sect;9.3) que estão autorizadas a aceitar e
    manter bens para o Debian. A avaliação e discussão que leva a
    tal decisão ocorre em uma lista de discussão eletrônica designada
    pelo(a) líder do Projeto ou seu(sua)(s) Delegado(a)(s), na qual qualquer
    desenvolvedor(a) pode enviar mensagens. Há um período mínimo de
    discussão de duas semanas antes que uma organização possa ser
    adicionada à lista de organizações de confiança.</p>
  </li>
</ol>

<h3>5.2. Nomeação</h3>

<ol>
  <li>O(A) líder do Projeto é eleito(a) pelos(as) desenvolvedores(as).</li>

  <li>A eleição começa seis semanas antes do posto de liderança
  ficar vago, ou (se já é muito tarde) imediatamente.</li>

  <li>Pela primeira semana qualquer desenvolvedor(a) pode
  nomear a si próprio(a) como um(a) candidato(a) a líder do Projeto, e resumir
  seus planos para seu mandato.</li>

  <li>Por três semanas após isto nenhum(a) candidato(a) pode ser nomeado(a);
  os(as) candidatos(as) deveriam usar este tempo para fazer campanha e debate.
  Se não há candidatos(as) ao fim do período de nomeação então o período de
  nomeação é estendido por mais uma semana, repetidamente se necessário.</li>

  <li>As próximas duas semanas são o período de eleição durante o qual os(as)
  desenvolvedores(as) podem enviar seus votos. Votos nas eleições de
  liderança são mantidos em segredo, mesmo após o término das eleições.</li>

  <li>As opções das cédulas serão aqueles(as) candidatos(as) que se nomearam e
  não desistiram ainda, mais <q>Nenhuma das acima</q>. Se <q>Nenhuma das
  acima</q> ganhar a eleição então o procedimento de eleição é repetido, muitas
  vezes se necessário.</li>

  <li>
       A decisão será tomada usando o método especificado na seção
       &sect;A.6 do Procedimento de Resolução Padrão. O quorum é o
       mesmo que para uma Resolução Geral (&sect;4.2) e a opção padrão
       é <q>Nenhuma das acima</q>.
  </li>

  <li>O(A) líder do Projeto serve por um ano a partir de sua eleição.</li>
</ol>

<h3>5.3. Procedimento</h3>

<p>O(A) líder do Projeto deveria tentar tomar decisões que são consistentes
com o consenso das opiniões dos(as) desenvolvedores(as).</p>

<p>Onde for possível, o(a) líder do Projeto deveria informalmente solicitar os
pontos de vista dos(as) desenvolvedores(as).</p>

<p>O(A) líder do Projeto deveria evitar super enfatizar seu próprio ponto
de vista quando tomando decisões em sua qualidade de líder.</p>

<toc-add-entry name="item-6">6. Comitê Técnico</toc-add-entry>

<h3>6.1. Poderes</h3>

<p>O <a href="tech-ctte">Comitê Técnico</a> pode:</p>

<ol>
  <li>
    <p>Decidir sobre qualquer problema de política técnica.</p>

    <p>Isso inclui o conteúdo dos manuais de políticas técnicas, materiais
    de referência dos(as) desenvolvedores(as), pacotes de exemplo e o
    comportamento de ferramentas de construção de pacotes não experimentais.
    (Em cada caso o(a) mantenedor(a) usual do programa relevante ou da
    documentação toma as decisões inicialmente, no entanto; veja 6.3(5).)</p>
  </li>

  <li>
    <p>Decidir qualquer assunto técnico onde há sobreposição das jurisdições
    dos(as) desenvolvedores(as).</p>

    <p>Em casos onde os(as) desenvolvedores(as) precisam implementar políticas
    técnicas compatíveis ou pontos de vista (por exemplo, se eles(as) não
    concordam sobre as prioridades de pacotes conflitantes, ou sobre o(a)
    dono(a) de um nome de comando, ou sobre qual pacote é responsável por
    um bug que ambos os(as) mantenedores(as) concordam ser um bug, ou sobre quem
    deveria ser o(a) mantenedor(a) de um pacote)) o comitê técnico pode resolver
    a questão.</p>
  </li>

  <li>
    <p>Tomar uma decisão quando requisitado para tal.</p>

    <p>Qualquer pessoa ou grupo pode delegar uma decisão própria ao
    comitê técnico, ou procurar aconselhamento com ele.</p>
  </li>

  <li>
    <p>Anular um(a) desenvolvedor(a) (requer uma maioria de 3:1).</p>

    <p>O comitê técnico pode pedir a um(a) desenvolvedor(a) para tomar um
    curso de ação técnica em particular mesmo que o(a) desenvolvedor(a)
    não queira; isto requer uma maioria de 3:1. Por exemplo, o
    Comitê pode determinar que uma reclamação feita pelo(a) emissor(a)
    de um bug é justificada e que a solução proposta pelo(a) emissor(a)
    deve ser implementada.</p>
  </li>

  <li>
    <p>Oferecer conselhos.</p>

    <p>O comitê técnico pode fazer anúncios formais sobre seus pontos de vista
    sobre qualquer questão. <cite>Membros individuais podem, é claro, criar
    declarações informais sobre seus pontos de vista sobre os prováveis pontos
    de vista do Comitê.</cite></p>
  </li>

  <li>
    <p>Juntamente com o(a) líder do Projeto, nomear novos membros para si mesmo
    ou remover membros existentes. (Veja &sect;6.2.)</p>
  </li>

  <li>
    <p>Nomear o(a) presidente do comitê técnico.</p>

    <p>
       O(A) presidente é eleito(a) pelo Comitê a partir de seus membros. Todos
       os membros do Comitê estão automaticamente nomeados; o Comitê começa
       a votar uma semana antes do posto ficar vago (ou imediatamente, se
       já é muito tarde). Os membros podem votar por aclamação pública em
       qualquer colega membro do Comitê, incluindo a si mesmos; não há opção
       padrão. A votação acaba quando todos os membros votaram, ou quando o
       período de votação acaba. O resultado é determinado usando o método
       especificado na seção A.6 do Procedimento de Resolução Padrão.
   </p>
  </li>

  <li>
    <p>O(A) presidente do Comitê pode servir de líder, juntamente com o(a)
    secretário(a)</p>

    <p>Como detalhado em &sect;7.1(2), o(a) presidente do comitê técnico
    e o(a) secretário(a) do Projeto podem, juntos, substituir o(a) líder se
    não houver líder.</p>
  </li>
</ol>

<h3>6.2. Composição</h3>

<ol>
  <li>
    <p>O comitê técnico consiste de até 8 desenvolvedores(as) e deveria
    ter normalmente pelo menos 4 membros.</p>
  </li>

  <li>
    <p>Quando há menos de 8 membros o comitê técnico pode recomendar
    membros novos ao(a) líder do Projeto, que pode escolher
    (individualmente) nomeá-los ou não.</p>
  </li>

  <li>
    <p>Quando houver 5 membros ou menos o comitê técnico pode nomear
    membros novos até que o número de membros atinja 6.</p>
  </li>

  <li>
    <p>Quando houver 5 membros ou menos por pelo menos uma semana o(a) líder
    do Projeto pode nomear novos membros até que o número de
    membros atinja 6, em intervalos de pelo menos uma semana por
    nomeação.</p>
  </li>

  <li>
    <p>Um(a) desenvolvedor(a) não é elegível para ser (re)nomeado(a) para o
    comitê técnico caso ele tenha sido um membro dentro dos 12 meses anteriores.
    </p>
  </li>

  <li>
    <p>Se o comitê técnico e o(a) líder do Projeto concordarem, eles
    podem remover ou substituir um membro existente do Comitê
    Técnico.</p>
  </li>

  <li>
    <p>Limite de mandato:</p>
    <ol>
      <li>
        <p>No dia 1º de janeiro de cada ano, o mandato de qualquer membro
        do Comitê que tenha servido por mais de 42 meses (3,5 anos) e que
        seja um dos dois membros mais antigos, é definido para expirar em
        31 de dezembro do mesmo ano.</p>
      </li>
      <li>
        <p>Um membro do comitê técnico é dito como mais antigo do que outro,
        se ele foi nomeado mais cedo, ou foram nomeados ao mesmo tempo e
        um tiver sido membro do Projeto Debian há mais tempo.
        No caso de um membro ter sido nomeado mais de uma vez, apenas a
        nomeação mais recente é relevante.</p>
      </li>
    </ol>
  </li>
</ol>

<h3>6.3. Procedimento</h3>

<ol>
  <li>
    <p>O comitê técnico usa o Procedimento de Resolução Padrão.</p>

    <p>Um rascunho de resolução ou emenda pode ser proposto por qualquer
    membro do comitê técnico. Não há período de discussão mínimo;
    o período de votação dura por uma semana ou até que o resultado
    não esteja mais em dúvida. Os membros podem mudar seus votos.
    Há um quorum de dois.</p>
  </li>

  <li>
    <p>Detalhes relacionados à votação</p>

    <p>O(A) presidente tem um voto de minerva. Quando o comitê técnico
    vota a anulação de um(a) desenvolvedor(a) que também é membro do
    Comitê, esse membro não pode votar (a menos que seja o(a)
    presidente, nesse caso ele(a) pode usar apenas seu voto de minerva).</p>
  </li>

  <li>
    <p>Discussão Pública e tomada de decisões.</p>

    <p>Discussão, rascunhos de resoluções e emendas, e votos dos membros
    do comitê, são publicados na lista de discussão pública do
    comitê técnico. Não há secretário(a) separado(a) para o Comitê.</p>
  </li>

  <li>
    <p>Confidencialidade das nomeações.</p>

    <p>O comitê técnico pode manter discussões confidenciais via e-mail
    privado ou em uma lista de discussão privada ou em outros meios para
    discutir nomeações para o Comitê. No entanto, as votações para as
    nomeações devem ser públicas.</p>
  </li>

  <li>
    <p>Ausência no desenvolvimento detalhado.</p>

    <p>O comitê técnico não se envolve no desenvolvimento de novas propostas e
    políticas. Tal trabalho deveria ser conduzido por indivíduos
    privadamente ou em conjunto e discutidos em fóruns ordinários
    de desenho e políticas técnicas.</p>

    <p>O comitê técnico se restringe a escolher ou adotar
    compromissos entre soluções e decisões que foram propostas e
    razoavelmente discutidas em outros lugares.</p>

    <p><cite>Membros individuais do comitê técnico podem, é claro,
    participar por conta própria em quaisquer aspectos do trabalho de
    desenvolvimento e políticas.</cite></p>
  </li>

  <li>
    <p>O comitê técnico toma decisões apenas como último recurso.</p>

    <p>O comitê técnico não toma uma decisão técnica até que esforços
    para se resolver a questão via consenso tenham sido feitos e
    falhado, a menos que ele tenha sido solicitado para tomar uma decisão
    pela pessoa ou grupo que seria normalmente responsável por ela.</p>
  </li>
</ol>

<toc-add-entry name="item-7">7. O(A) secretário(a) do Projeto</toc-add-entry>

<h3>7.1. Poderes</h3>

<p>O(A) <a href="secretary">Secretário(a)</a>:</p>

<ol>
  <li>
    <p>Obtém os votos entre os(a) desenvolvedores(as) e determina o número
    e a identidade dos(as) desenvolvedores(as), sempre que requerido pela
    constituição.</p>
  </li>

  <li>
    <p>Pode servir como líder do Projeto, junto com o(a) presidente do Comitê
    Técnico.</p>

    <p>Se não há líder do Projeto então o(a) presidente do comitê técnico
    e o(a) secretário(a) do Projeto podem através de concordância mútua tomar
    decisões se eles(as) considerarem imperativo fazê-lo.</p>
  </li>

  <li>
    <p>Resolve qualquer disputa sobre a interpretação da constituição.</p>
  </li>

  <li>
    <p>Pode delegar parte ou toda a sua autoridade para alguém, ou
    desistir dessa delegação a qualquer momento.</p>
  </li>
</ol>

<h3>7.2. Nomeação</h3>

<p>O(A) secretário(a) do Projeto é nomeado(a) pelo(a) líder do Projeto e pelo(a)
secretário(a) do Projeto atual.</p>

<p>Se o(a) líder do Projeto e o(a) secretário(a) do Projeto atuais não podem
concordar em uma nova nomeação eles(as) devem pedir aos(as) desenvolvedores(as)
via uma Resolução Geral que nomeiem um(a) secretário(a).</p>

<p>Se não há secretário(a) do Projeto ou o(a) secretário(a) atual está
indisponível e não delegou autoridade para uma decisão, então a decisão pode ser
tomada ou delegada pelo(a) presidente do comitê técnico, como secretário(a)
interino(a).</p>

<p>O mandato do(a) secretário(a) do Projeto é de 1 ano, depois do qual
ele(a) ou outro(a) secretário(a) deve ser (re)nomeado(a).</p>

<h3>7.3. Procedimento</h3>

<p>O(A) secretário(a) do Projeto deveria tomar decisões que são honestas e
razoáveis, e preferivelmente consistentes com o consenso dos(as)
desenvolvedores(as).</p>

<p>Quando agindo conjuntamente para substituir um(a) líder do Projeto ausente,
o(a) presidente do comitê técnico e o(a) secretário(a) do Projeto deveriam tomar
decisões somente quando absolutamente necessárias e somente quando
consistentes com o consenso dos(as) desenvolvedores(as).</p>

<toc-add-entry name="item-8">8. Os(As) Delegados(as) do(a) líder do Projeto
</toc-add-entry>

<h3>8.1. Poderes</h3>

<p>Os(As) Delegados(as) do(a) líder do Projeto:</p>

<ol>
  <li>têm poderes delegados a eles(as) pelo(a) líder do Projeto;</li>

  <li>podem tomar certas decisões que o(a) líder não pode tomar diretamente,
  incluindo aprovação ou expulsão de desenvolvedores(as) ou designação
  de pessoas como desenvolvedores(as) que não mantêm pacotes. <cite>Isto
  é para evitar concentração de poder, particularmente sobre a qualidade
  de membro como um(a) desenvolvedor(a), nas mãos do(a) líder do Projeto.</cite></li>
</ol>

<h3>8.2. Nomeação</h3>

<p>Os(As) Delegados(as) são nomeados(as) pelo(a) líder do Projeto e podem ser
substituídos(as) pelo(a) líder a critério do(a) próprio(a) líder. O(A) líder do
Projeto não pode tornar a posição como um(a) Delegado(a) condicional às
decisões particulares do(a) Delegado(a), nem pode anular uma decisão
tomada por um(a) Delegado(a) uma vez que esteja tomada.</p>

<h3>8.3. Procedimento</h3>

<p>Delegados(as) podem tomar decisões como acharem melhor mas deveriam tentar
implementar decisões técnicas boas e/ou seguir a opinião consensual.</p>

<toc-add-entry name="item-9">9. Bens mantidos em confiança para o Debian
</toc-add-entry>

<p>Na maioria das jurisdições ao redor do mundo, o projeto Debian não está
em posição de diretamente manter fundos ou outras propriedades. Portanto,
propriedades têm que ser mantidas por uma das várias organizações conforme
detalhado em &sect;9.2.</p>

<p>Tradicionalmente, a SPI era a única organização autorizada a guardar
propriedades e dinheiro para o Projeto Debian. A SPI foi criada nos EUA
para guardar dinheiro em confiança lá.</p>

<p>A <a href="https://www.spi-inc.org">SPI</a> e o Debian são organizações
separadas que compartilham alguns objetivos.
O Debian é grato pela infraestrutura legal de suporte oferecida pela SPI.</p>

<h3>9.1. Relacionamento com organizações associadas</h3>

<ol>
  <li>
    <p>Desenvolvedores(as) Debian não se tornam agentes ou empregados(as)
    de organizações mantendo bens em confiança para o Debian, ou
    uns dos outros, ou de pessoas com autoridade no Projeto Debian,
    somente pela virtude de serem desenvolvedores(as) Debian. Uma pessoa
    agindo como um(a) desenvolvedor(a) o faz como indivíduo, em nome
    próprio. Tais organizações podem, de acordo próprio,
    estabelecer relacionamentos com indivíduos que também sejam
    desenvolvedores(as) Debian.</p>
  </li>
</ol>

<h3>9.2. Autoridade</h3>

<ol>
  <li>
    <p>Uma organização mantendo bens para o Debian não tem autoridade
    sobre decisões técnicas ou não técnicas do Debian, exceto que
    nenhuma decisão do Debian com respeito a quaisquer propriedades
    mantidas pela organização irá requerer que a mesma atue fora de sua
    própria autoridade legal.</p>
  </li>
  <li>
    <p>O Debian não clama por autoridade sobre uma organização que mantém
    bens para o Debian além da autoridade sobre o uso das propriedades
    mantidas em confiança para o Debian.</p>
  </li>
</ol>

<h3>9.3. Organizações confiáveis</h3>

<p>Quaisquer doações para o Projeto Debian devem ser feitas para qualquer
uma das organizações designadas pelo(a) líder do Projeto (ou um(a) Delegado(a))
para serem autorizadas a manusear bens a serem usados para o Projeto
Debian.</p>

<p>Organizações mantendo bens em confiança para o Debian devem se encarregar
das obrigações razoáveis para o manuseio de tais bens.</p>

<p>O Debian mantém pública uma lista de organizações de confiança que
aceitam doações e mantêm bens em confiança para o Debian
(incluindo ambos propriedade tangível e propriedade intelectual)
que inclui os compromissos que essas organizações fizeram sobre
como estes bens serão manuseados.</p>

<toc-add-entry name="item-A">A. Procedimento padrão para resolução
</toc-add-entry>

<p>Estas regras aplicam-se a tomadas de decisões comunais por comitês e
plebiscitos, onde declarado acima.</p>

<h3>A.0. Proposta</h3>

<p>O procedimento formal começa quando um rascunho de resolução é
proposto e apadrinhado, como requerido.</p>

<h3>A.1. Discussão e emendas</h3>

<ol>
  <li>Seguindo a proposta, a resolução pode ser discutida. Emendas devem
  ser tornadas formais sendo propostas e apadrinhadas de acordo
  com os requerimentos para uma nova resolução ou diretamente pelo(a)
  proponente da resolução original.</li>

  <li>Uma emenda formal pode ser aceita pelo(a) proponente da resolução,
  neste caso o rascunho da resolução formal é imediatamente alterado
  para ficar igual.</li>

  <li>Se uma emenda formal não é aceita ou um(a) dos(as) padrinhos/madrinhas da
  resolução não concorda com a aceitação pelo(a) proponente de um emenda
  formal, a emenda continua como uma emenda e será votado.</li>

  <li>Se uma emenda aceita pelo(a) proponente original não é do gosto
  dos(as) outros(as), eles(as) podem propor outra emenda para reverter a
  mudança feita anteriormente (novamente, eles(as) precisam atingir
  os requerimentos para proponente e padrinho(s)/madrinha(as).)</li>

  <li>O(A) proponente de uma resolução pode sugerir mudanças para os dizeres
  das emendas; estes tomam efeito se o(a) proponente da emenda
  concorda e nenhum(a) dos(as) padrinhos/madrinhas se opõe. Neste caso a emenda
  alterada será votada ao invés da original</li>

  <li>O(A) proponente de uma resolução pode fazer mudanças para correção de
  erros pequenos (por exemplo, erros tipográficos ou inconsistências)
  ou mudanças que não alteram o significado, desde que ninguém se oponha
  dentro de 24 horas. Neste caso, o período de discussão mínimo não
  é reiniciado.</li>
</ol>

<h3>A.2. Chamada para uma votação</h3>

<ol>
  <li>O(A) proponente ou um(a) padrinho/madrinha de uma moção ou uma emenda
  pode chamar para uma votação, desde que o período de discussão mínimo (se
  houver) tenha terminado.</li>

  <li>
    O(A) proponente ou qualquer padrinho/madrinha de uma resolução podem chamar
    para uma votação nessa resolução e em todas a emendas relacionadas.
  </li>

  <li>A pessoa que chama para uma votação diz o que acha que os
  dizeres da resolução e quaisquer emendas relevantes devam ser e,
  consequentemente, a forma que a votação deve tomar. No entanto,
  a decisão final na forma da(s) votação(ões) é do(a) secretário(a) -
  veja 7.1(1), 7.1(3) e A.3(4).</li>

  <li>
       O período mínimo de discussão é contado a partir do momento em que
       a última emenda formal é aceita, ou do momento em que a resolução
       completa foi proposta se nenhuma emenda foi proposta e aceita.
  </li>
</ol>

<h3>A.3. Procedimento de votação</h3>

<ol>
  <li>
       Cada resolução e suas emendas relacionadas são votadas em uma única
       cédula que inclui uma opção para a resolução original, cada emenda,
       e a opção padrão (onde aplicável).
  </li>

  <li>
       A opção padrão não deve ter nenhum requerimento de supermaioria.
       Opções que não tiverem um requerimento explícito de supermaioria
       têm um requerimento de maioria 1:1.
  </li>

  <li>
       Os votos são contados de acordo com as regras em A.6. A opção padrão é
       <q>Mais discussões</q>, a menos que seja especificado de outra maneira.
  </li>

  <li>Em casos de dúvida, o(a) secretário(a) do Projeto decidirá os problemas de
  procedimento.</li>
</ol>

<h3>A.4. Retirando resoluções ou emendas não aceitas</h3>

<p>O(A) proponente de uma resolução ou emenda não aceita pode retirá-la.
Nesse caso novos(as) proponentes podem vir e mantê-la viva, nesse caso a
primeira pessoa a fazê-lo torna-se a nova proponente e quaisquer outros
se tornam padrinhos/madrinhas se já não são.</p>

<p>Um(a) padrinho/madrinha de uma resolução ou emenda (a menos que ela tenha
sido aceita) pode retirá-la.</p>

<p>Se a retirada do(a) proponente e/ou padrinhos/madrinhas significa que uma
resolução não tem proponente ou padrinhos/madrinhas o suficiente ela não será
votada a menos que isso seja retificado antes do vencimento da resolução.</p>

<h3>A.5. Expiração</h3>

<p>
   Se uma resolução proposta não foi discutida, emendada, votada ou de
   outra forma manejada por 4 semanas, o(a) secretário(a) pode emitir uma
   declaração de que a questão está sendo retirada. Se nenhum(a) dos(as)
   padrinhos/madrinhas de qualquer uma das propostas discordar em uma semana, a
   questão é retirada.
</p>

<p>
   O(A) secretário(a) também pode incluir sugestões de como proceder, se
   apropriado.
</p>

<h3>A.6. Contagem de votos</h3>

<ol>
   <li> A cédula de cada votante gradua as opções que estão sendo votadas.
        Nem todas as opções precisam ser graduadas. Opções graduadas são
        consideradas preferidas em relação às não graduadas. Votantes podem
        graduar opções igualmente. Opções não graduadas são consideradas como
        graduadas igualmente entre si. Detalhes de como as cédulas podem ser
        preenchidas serão incluídas na <q>Chamada para votos</q>.
   </li>
   <li> Se a votação tiver um requerimento de quorum R quaisquer opções que
        não sejam a padrão que não receberem pelo menos R votos graduando
        aquela opção acima da padrão são desconsideradas.
   </li>
   <li> Qualquer opção (não padrão) que não vencer a opção padrão na sua
        proporção requerida de maioria é desconsiderada.
        <ol>
             <li>
                  Dadas duas opções A e B, V(A,B) é o número de votantes que
                  preferem a opção A sobre a opção B.
             </li>
             <li>
                  Uma opção A vence a opção padrão D por uma proporção de
                  maioria N, se V(A,D) for maior ou igual que N * V(D,A) e
                  V(A,D) for estritamente maior que V(D,A).
             </li>
             <li>
                  Se uma supermaioria de S:1 é requerida para A, sua
                  proporção de maioria é S; caso contrário, sua proporção
                  de maioria é 1.
             </li>
        </ol>
   </li>
   <li> Da lista de opções consideradas, nós geramos uma lista de vitórias
        em pares.
        <ol>
             <li>
                  Uma opção A vence uma opção B se V(A,B) for estritamente
                  maior que V(B,A)
             </li>
        </ol>
   </li>
   <li> Da lista de vitórias em pares [consideradas], nós geramos um
        conjunto de vitórias transitivas.
        <ol>
             <li>
                  Uma opção A vence transitivamente uma opção C se A vencer C
                  ou se há alguma outra opção B onde A vence B, e B
                  transitivamente vence C.
             </li>
        </ol>
   </li>
   <li> Nós construímos o conjunto Schwartz a partir do conjunto de vitórias
        transitivas.
        <ol>
             <li>
                  Uma opção A está no conjunto Schwartz se para todas as opções
                  B, A vence transitivamente B, ou B não vence transitivamente
                  A.
             </li>
        </ol>
   </li>
   <li> Se houver vitórias entre opções no conjunto Schwartz, nós removemos
        a mais fraca destas vitórias da lista de vitórias em pares, e
        retornamos para o passo 5.
        <ol>
             <li>
                  Uma vitória (A,X) é mais fraca que uma vitória (B,Y) se
                  V(A,X) for menor que V(B,Y). Além disso, (A,X) é mais fraca
                  que (B,Y) se V(A,X) é igual a V(B,Y) e V(X,A) é maior que
                  V(Y,B).
             </li>
             <li>
                  Uma vitória fraquíssima é aquela que não possui uma vitória
                  mais fraca que ela. Pode haver mais que uma destas vitórias.
             </li>
        </ol>
   </li>
   <li> Se não houver vitórias dentro do conjunto Schwartz, então o vencedor
        é escolhido a partir das opções do conjunto Schwartz. Se houver apenas
	uma dessas opções, esta é a vencedora. Se houver múltiplas opções, o(a)
	eleitor(a) com o voto de minerva escolhe qual das opções ganha.
   </li>
</ol>

<p>
 <strong>Nota:</strong> Opções que os(as) votantes graduam acima da opção padrão
 são opções que eles(as) acham aceitáveis. Opções graduadas abaixo da opção
 padrão são opções que eles(as) acham inaceitáveis.
</p>

<p><cite>Quando o Procedimento de Resolução Padrão vai ser usado,
o texto que se refere a ele deve especificar o que é suficiente para
se ter um rascunho de resolução proposto e/ou apadrinhado, qual o
período de discussão mínimo e qual o período mínimo de votação.
Deve também especificar qualquer supermaioria e/ou quorum (e opção
padrão) a ser usado.</cite></p>

<toc-add-entry name="item-B">B. Uso de linguagem e tipografia</toc-add-entry>

<p>O presente do indicativo (<q>é</q>, por exemplo) significa que a
declaração é uma regra nesta constituição. <q>Pode</q> indica que a
pessoa ou grupo tem liberdade de ação. <q>Deveria</q> significa que
será considerado uma boa coisa se a sentença for obedecida, mas não
é obrigatória. <cite>Texto marcado como citação, como este, é uma
análise racional e não é parte da constituição. Ele pode ser usado
apenas para ajudar na interpretação em casos duvidosos.</cite></p>
