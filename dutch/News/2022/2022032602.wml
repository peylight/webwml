#use wml::debian::translation-check translation="9264c23e43e374bf590c29166dde81515943b562"
<define-tag pagetitle>Debian 10 is bijgewerkt: 10.12 werd uitgebracht</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de twaalfde update aan van zijn
eerdere stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>



<h2>Aangescherpte controle van het OpenSSL-handtekeningalgoritme</h2>

<p>De update van OpenSSL in deze tussenrelease bevat een wijziging om te
garanderen dat het gevraagde handtekeningalgoritme ondersteund wordt door het
actieve beveiligingsniveau.</p>

<p>Hoewel dit op de meeste toepassingen geen invloed zal hebben, zou dit
kunnen leiden tot foutmeldingen als een niet-ondersteund algoritme wordt
gevraagd - bijvoorbeeld het gebruik van RSA+SHA1-handtekeningen met het
standaard beveiligingsniveau 2.</p>

<p>In dergelijke gevallen moet het beveiligingsniveau uitdrukkelijk worden
verlaagd, hetzij voor individuele verzoeken, hetzij meer in het algemeen.
Hiervoor kunnen wijzigingen in de configuratie van toepassingen nodig zijn.
Voor OpenSSL zelf kan per verzoek het beveiligingsniveau verlaagd worden met
een commandoregeloptie zoals:</p>

<p>-cipher <q>ALL:@SECLEVEL=1</q></p>

<p>met de relevante configuratie op systeemniveau in /etc/ssl/openssl.cnf</p>


<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van oldstable, de eerdere stabiele release, voegt een paar
belangrijke correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apache-log4j1.2 "Oplossen van beveiligingsproblemen [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307], door ondersteuning voor de modules JMSSink, JDBCAppender, JMSAppender en Apache Chainsaw te verwijderen">
<correction apache-log4j2 "Oplossen van probleem met code-uitvoering van op afstand [CVE-2021-44832]">
<correction atftp "Probleem met een informatielek oplossen [CVE-2021-46671]">
<correction base-files "Update voor tussenrelease 10.12">
<correction beads "Hercompileren tegen een bijgewerkt cimg om verschillende stapelbufferoverlopen te herstellen [CVE-2020-25693]">
<correction btrbk "Regressie in de update voor CVE-2021-38173 herstellen">
<correction cargo-mozilla "Nieuw pakket, teruggecompileerd vanaf Debian 11, om de compilatie van nieuwe rust-versies te ondersteunen">
<correction chrony "Het lezen van het chronyd-configuratiebestand dat timemaster(8) genereert, toestaan">
<correction cimg "Problemen van stapelbufferoverloop oplossen [CVE-2020-25693]">
<correction clamav "Nieuwe bovenstroomse stabiele release; probleem met denial of service oplossen [CVE-2022-20698]">
<correction cups "Oplossing voor <q>een invoervalidatieprobleem kan een kwaadwillige toepassing in staat stellen besloten geheugen te lezen</q> [CVE-2020-10001]">
<correction debian-installer "Hercompilatie tegen oldstable-proposed-updates; kernel-ABI updaten naar -20">
<correction debian-installer-netboot-images "Hercompilatie tegen oldstable-proposed-updates">
<correction detox "Reparatie voor het verwerken van grote bestanden op ARM-architecturen">
<correction evolution-data-server "Crash bij verkeerd opgemaakte serverreactie verhelpen [CVE-2020-16117]">
<correction flac "Oplossing voor een probleem van lezen buiten de grenzen [CVE-2020-0499]">
<correction gerbv "Oplossing voor probleem met code-uitvoering [CVE-2021-40391]">
<correction glibc "Importeren van verschillende probleemoplossingen vanuit de bovenstroomse stabiele tak; vereenvoudigde controle op ondersteunde kernelversies, aangezien 2.x-kernels niet langer worden ondersteund; ondersteuning van installatie op kernels met een releasenummer groter dan 255">
<correction gmp "Oplossing voor probleem van integer- en bufferoverloop [CVE-2021-43618]">
<correction graphicsmagick "Oplossing voor probleem van bufferoverloop [CVE-2020-12672]">
<correction htmldoc "Oplossing voor probleem met lezen buiten de grenzen [CVE-2022-0534] en problemen van bufferoverloop [CVE-2021-43579 CVE-2021-40985]">
<correction http-parser "Oplossing voor onbedoelde ABI-breuk">
<correction icu "Hulpprogramma <q>pkgdata</q> repareren">
<correction intel-microcode "Update van ingesloten microcode; enkele beveiligingsproblemen inperken [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction jbig2dec "Oplossing voor probleem van bufferoverloop [CVE-2020-12268]">
<correction jtharness "Nieuwe bovenstroomse versie om de compilatie van recentere OpenJDK-11-versies te ondersteunen">
<correction jtreg "Nieuwe bovenstroomse versie om de compilatie van recentere OpenJDK-11-versies te ondersteunen">
<correction lemonldap-ng "Verificatieproces repareren in plug-ins voor het testen van wachtwoorden [CVE-2021-20874]; aanbevelen van gsfonts toevoegen om captcha te herstellen">
<correction leptonlib "Probleem van denial of service oplossen [CVE-2020-36277], problemen van bufferoverschrijding bij lezen oplossen [CVE-2020-36278 CVE-2020-36279 CVE-2020-36280 CVE-2020-36281]">
<correction libdatetime-timezone-perl "Update van meegeleverde data">
<correction libencode-perl "Oplossen van geheugenlek in Encode.xs">
<correction libetpan "Probleem met STARTTLS-reactie-injectie oplossen [CVE-2020-15953]">
<correction libextractor "Ongeldig leesprobleem opgelost [CVE-2019-15531]">
<correction libjackson-json-java "Problemen met code-uitvoering opgelost [CVE-2017-15095 CVE-2017-7525], externe entiteitskwesties met XML [CVE-2019-10172]">
<correction libmodbus "Oplossing voor problemen met lezen buiten het bereik [CVE-2019-14462 CVE-2019-14463]">
<correction libpcap "Controleren van de lengte van de PHB-header voordat deze gebruikt wordt om geheugen toe te wijzen [CVE-2019-15165]">
<correction libsdl1.2 "Invoerfocusgebeurtenissen op de juiste manier afhandelen; problemen van bufferoverloop [CVE-2019-13616 CVE-2019-7637] en bufferoverschrijding bij lezen [CVE-2019-7572 CVE-2019-7573 CVE-2019-7574 CVE-2019-7575 CVE-2019-7576 CVE-2019-7577 CVE-2019-7578 CVE-2019-7635 CVE-2019-7636 CVE-2019-7638] oplossen">
<correction libxml2 "Probleem met gebruik-na-vrijgave oplossen [CVE-2022-23308]">
<correction linux "Nieuwe bovenstroomse stabiele release; [rt] update naar 4.19.233-rt105; verhogen van ABI naar 20">
<correction linux-latest "Update naar ABI 4.19.0-20">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; [rt] update naar 4.19.233-rt105; verhogen van ABI naar 20">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; [rt] update naar 4.19.233-rt105; verhogen van ABI naar 20">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; [rt] update naar 4.19.233-rt105; verhogen van ABI naar 20">
<correction llvm-toolchain-11 "Nieuw pakket, teruggecompileerd vanuit Debian 11 om het bouwen van nieuwe rust-versies te ondersteunen">
<correction lxcfs "Foutief rapporteren van swap-gebruik oplossen">
<correction mailman "Probleem met cross-site scripting oplossen [CVE-2021-43331]; oplossing voor <q>een moderator van een lijst kan het beheerderswachtwoord van de lijst, gecodeerd in een CSRF-token, kraken</q> [CVE-2021-43332]; reparatie voor een mogelijke CSRF-aanval tegen een lijstbeheerder van een lid of moderator van de lijst [CVE-2021-44227]; reparatie van regressies in reparaties voor CVE-2021-42097 en CVE-2021-44227">
<correction mariadb-10.3 "Nieuwe bovenstroomse stabiele release; beveiligingsmaatregelen [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction node-getobject "Probleem met prototypevervuiling oplossen [CVE-2020-28282]">
<correction opensc "Problemen van toegang buiten het bereik oplossen [CVE-2019-15945 CVE-2019-15946], crash door het lezen van onbekend geheugen [CVE-2019-19479], probleem van dubbele vrijgave [CVE-2019-20792], bufferoverloopproblemen [CVE-2020-26570 CVE-2020-26571 CVE-2020-26572]">
<correction openscad "Oplossen van bufferoverlopen in de STL-parser [CVE-2020-28599 CVE-2020-28600]">
<correction openssl "Nieuwe bovenstroomse release">
<correction php-illuminate-database "Probleem met query-binding oplossen [CVE-2021-21263], probleem met SQL-injectie bij gebruik met de Microsoft SQL Server">
<correction phpliteadmin "Probleem met cross-site scripting oplossen [CVE-2021-46709]">
<correction plib "Probleem van integeroverloop oplossen [CVE-2021-38714]">
<correction privoxy "Reparatie voor geheugenlek [CVE-2021-44540] en probleem van cross-site scripting [CVE-2021-44543]">
<correction publicsuffix "Bijgewerkte meegeleverde data">
<correction python-virtualenv "Poging om pkg_resources vanuit PyPI te installeren, verhinderen">
<correction raptor2 "Oplossen van probleem van array-toegang buiten het bereik [CVE-2020-25713]">
<correction ros-ros-comm "Een probleem van denial of service oplossen [CVE-2021-37146]">
<correction rsyslog "Stapeloverloopproblemen oplossen [CVE-2019-17041 CVE-2019-17042]">
<correction ruby-httpclient "Systeemcertificaatarchief gebruiken">
<correction rust-cbindgen "Nieuwe bovenstroomse stabiele release om de bouw van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction rustc-mozilla "Nieuw bronpakket om de bouw van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction s390-dasd "Ophouden met het doorgeven van de verouderde optie -f aan dasdfmt">
<correction spip "Probleem met cross-site scripting oplossen">
<correction tzdata "Bijwerken van de data voor Fiji en Palestina">
<correction vim "Reparatie van mogelijkheid om code uit te voeren in beperkte modus [CVE-2019-20807], problemen van bufferoverloop [CVE-2021-3770 CVE-2021-3778 CVE-2021-3875], probleem van gebruik na vrijgave [CVE-2021-3796]; verwijderen van per ongeluk toegevoegde patch">
<correction wavpack "Gebruik van niet-geïnitialiseerde waarden corrigeren [CVE-2019-1010317 CVE-2019-1010319]">
<correction weechat "Oplossing voor verschillende denial of service-problemen [CVE-2020-8955 CVE-2020-9759 CVE-2020-9760 CVE-2021-40516]">
<correction wireshark "Oplossen van verschillende beveiligingsproblemen in dissectors [CVE-2021-22207 CVE-2021-22235 CVE-2021-39921 CVE-2021-39922 CVE-2021-39923 CVE-2021-39924 CVE-2021-39928 CVE-2021-39929]">
<correction xterm "Oplossing voor probleem van bufferoverloop [CVE-2022-24130]">
<correction zziplib "Oplossing voor probleem van denial of service [CVE-2020-18442]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de eerdere
stabiele release. Het beveiligingsteam heeft voor elk van deze updates al een
advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2019 4513 samba>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4989 strongswan>
<dsa 2021 4990 ffmpeg>
<dsa 2021 4991 mailman>
<dsa 2021 4993 php7.3>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4997 tiff>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5005 ruby-kaminari>
<dsa 2021 5006 postgresql-11>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5014 icu>
<dsa 2021 5015 samba>
<dsa 2021 5016 nss>
<dsa 2021 5018 python-babel>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5021 mediawiki>
<dsa 2021 5022 apache-log4j2>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5032 djvulibre>
<dsa 2022 5035 apache2>
<dsa 2022 5036 sphinxsearch>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5043 lxml>
<dsa 2022 5047 prosody>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5065 ipython>
<dsa 2022 5066 ruby2.5>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5078 zsh>
<dsa 2022 5081 redis>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5093 spip>
<dsa 2022 5096 linux-latest>
<dsa 2022 5096 linux-signed-amd64>
<dsa 2022 5096 linux-signed-arm64>
<dsa 2022 5096 linux-signed-i386>
<dsa 2022 5096 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5103 openssl>
<dsa 2022 5105 bind9>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction angular-maven-plugin "Niet langer bruikbaar">
<correction minify-maven-plugin "Niet langer bruikbaar">

</table>

<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze
tussenrelease in de eerdere stabiele release opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable (de eerdere stabiele release):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
distributie op &lt;debian-release@lists.debian.org&gt;.</p>


