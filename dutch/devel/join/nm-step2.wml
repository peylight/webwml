#use wml::debian::template title="Stap 2: Identificatie" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="c4faf7d38264352c8c2861edac022312c173ab7e"

<p>De informatie op deze pagina, hoewel openbaar, zal voornamelijk van belang
zijn voor toekomstige Debian-ontwikkelaars.</p>


<h2>Stap 2: Identificatie</h2>

<h3>Waarom OpenPGP?</h3>

<p>Debian maakt uitgebreid gebruik van OpenPGP omdat <a
href="newmaint#Member">de leden van Debian</a> over de hele wereld verspreid
zijn (zie de <a href="$(DEVEL)/developers.loc">locatie van ontwikkelaars</a>)
en elkaar zelden persoonlijk ontmoeten. Dit betekent dat er geen vertrouwen kan
worden opgebouwd door persoonlijk contact en dat andere middelen noodzakelijk
zijn. Alle ontwikkelaars van Debian worden geïdentificeerd aan de hand van hun
<a href="https://openpgp.org/">OpenPGP</a>-sleutel. Deze sleutels maken het
mogelijk om berichten en andere gegevens te authenticeren door deze te
ondertekenen. Raadpleeg voor meer informatie over OpenPGP-sleutels het
README-bestand in het pakket <code>debian-keyring</code>.</p>

<h3>Een sleutel verstrekken</h3>

<p>Iedere <a href="newmaint#Applicant">kandidaat</a> moet een openbare sleutel
van OpenPGP versie 4 met versleutelingsmogelijkheden verstrekken. De beste
manier om dit te doen is er een te exporteren naar een van de servers voor
openbare sleutels, zoals <tt>keys.openpgp.org</tt>.
Een openbare sleutel kan geëxporteerd worden met:</p>
<pre>
gpg --send-key --keyserver &lt;serveradres&gt; &lt;uw_sleutel_id&gt;
</pre>

<p>Als uw sleutel geen versleutelingsmogelijkheden heeft, kunt u eenvoudig een
versleutelingssubsleutel toevoegen.</p>

<p>Zie <a href="https://keyring.debian.org/">keyring.debian.org</a>
voor meer informatie over sleutelformaten en -eigenschappen.</p>


<h3>Verificatie</h3>

<p>Aangezien iedereen een publieke sleutel naar de servers kan uploaden, moet
worden geverifieerd dat de sleutel aan de kandidaat toebehoort.</p>

<p>Om dit te bereiken moet de openbare sleutel worden ondertekend door twee
andere <a href="newmaint#Member">leden van Debian</a>. Daartoe moet de kandidaat
dit lid van Debian persoonlijk ontmoeten en zichzelf identificeren (door het
verstrekken van een paspoort, een rijbewijs of een ander identiteitsbewijs).</p>


<h4><a name="key_signature">Hoe u uw OpenPGP-sleutel kunt laten ondertekenen</a></h4>

<p>Er zijn verschillende manieren om een lid van Debian te vinden voor een
sleuteluitwisseling. U zou ze in de onderstaande volgorde moeten proberen:</p>

<ol>

<li>Aankondigingen van sleutelondertekeningsfeestjes worden meestal gepost
op de mailinglijst <code>debian-devel</code>; dus kijk daar eerst.</li>

<li><p>U kunt zoeken naar ontwikkelaars in specifieke streken via de <a
href="https://wiki.debian.org/Keysigning">coördinatiepagina voor
de ondertekening van sleutels</a>:</p>

<ul>
      <li>Eerst zou u in de lijst met aanbiedingen voor het ondertekenen van
      sleutels moeten zoeken naar een lid van Debian in uw buurt.</li>
      <li>Indien u onder de aanbiedingen voor het ondertekenen van sleutels
      geen lid van Debian kunt vinden, kunt u uw verzoek om ondertekening van
      een sleutel registreren.</li>
</ul>
</li>

<li>Als er gedurende enkele weken niemand op uw verzoek heeft gereageerd, stuur
dan een e-mail naar <email debian-private@lists.debian.org> en vertel hen
precies waar u woont (en vernoem ook enkele grote steden in uw buurt), dan
kunnen zij in de database van ontwikkelaars kijken of er ontwikkelaars bij u in
de buurt wonen.</li>

</ol>

<p>Zodra u iemand hebt gevonden om uw sleutel te ondertekenen, moet u de
stappen volgen in de <a href="$(HOME)/events/keysigning">Mini-HOWTO voor
sleutelondertekening</a>.</p>

<p>Het wordt aanbevolen dat u ook de sleutel van de ontwikkelaar van Debian
ondertekent. Dit is niet noodzakelijk voor de controle van uw ID, maar het
versterkt het web van vertrouwen.</p>


<h4>Wanneer u uw sleutel niet kunt laten ondertekenen</h4>

<p>Als alle bovenstaande stappen mislukken, neem dan contact op met de
<a href="newmaint#FrontDesk">frontdesk</a> en vraag om hulp. Zij kunnen u een
andere manier van identificatie aanbieden.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
