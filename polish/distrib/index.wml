#use wml::debian::template title="Jak zdobyć Debiana"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25"
#include "$(ENGLISHDIR)/releases/images.data"

# This is not exactly the same as the English sentence, but the best I can
# think of while being precise (free != unrestricted) and sane ("wolny" means
# "slow" in some contextx).
<p>Debian to <a href="../intro/free">wolna</a> dystrybucja dostępna w Internecie.
Możesz ją pobrać w całości z jednego z naszych
<a href="ftplist">serwerów lustrzanych</a>.
<a href="../releases/stable/installmanual">Podręcznik instalacji</a> zawiera
dokładne instrukcje.
<a href="../releases/stable/releasenotes">Tutaj</a> można znaleźć uwagi dotyczące wydania.
</p>

<p>Ta strona zawiera opcje instalacji wersji stabilnej Debiana. Jeżeli jesteś 
zainteresowany wersją testową lub niestabilną, zajrzyj na <a href="../releases/">stronę wydań</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Pobierz plik instalacyjny</a></h2>    
    <p>W zależności od twojego połączenia internetowego, możesz pobrać:</p>
    <ul>
      <li><a href="netinst"><strong>Mały plik obrazu</strong></a>:
	    może być pobrany szybko i powinien być nagrany na dysku wymiennym.
	    Aby z niego skorzystać potrzebujesz maszyny z połączeniem internetowym.
	<ul class="quicklist downlist">
	  <li><a title="Pobierz instalator dla 64-bitowego PC z procesorem Intel lub AMD"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">ISO 
	         netinst dla 64-bitowego PC</a></li>
	  <li><a title="Pobierz instalator dla zwykłego 32-bitowego PC z procesorem Intel lub AMD"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">ISO 
		 netinst dla 32-bitowego PC</a></li>
	</ul>
      </li>
      <li>Większy <a href="../CD/"><strong>kompletny plik obrazu
	</strong></a>: zawiera więcej pakietów, ułatwiając instalację na maszynie 
	bez połączenia internetowego.
	<ul class="quicklist downlist">
	  <li><a title="Pobierz torrenty DVD dla 64-bitowego PC z procesorem Intel lub AMD"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrenty dla 64-bitowego PC (DVD)</a></li>
	  <li><a title="Pobierz torrenty DVD dla zwykłego 32-bitowego PC z procesorem Intel lub AMD"
		 href="<stable-images-url/>/i386/bt-dvd/">torrenty dla 32-bitowego PC (DVD)</a></li>
	  <li><a title="Pobierz torrenty CD dla 64-bitowego PC z procesorem Intel lub AMD"
	         href="<stable-images-url/>/amd64/bt-cd/">torrenty dla 64-bitowego PC (CD)</a></li>
	  <li><a title="Pobierz torrenty DVD dla zwykłego 32-bitowego PC z procesorem Intel lub AMD"
		 href="<stable-images-url/>/i386/bt-cd/">torrenty dla 32-bitowego PC (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Użyj obrazu Debiana dla chmury</a></h2>
    <p>Oficjalny <a href="https://cloud.debian.org/images/cloud/"><strong> obraz do użycia w chmurze</strong></a>, 
        zbudowany przez zespół ds. chmury, może być użyty z:</p>
    <ul>
      <li>dostawcą OpenStack w formacie qcow2 lub raw.
      <ul class="quicklist downlist">
          <li>64-bit AMD/Intel (<a title="obraz qcow2 64-bit AMD/Intel dla OpenStack" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="obraz raw 64-bit AMD/Intel dla OpenStack" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="obraz qcow2 64-bit ARM dla OpenStack" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="obraz raw 64-bit ARM dla OpenStack" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
          <li>64-bit Little Endian PowerPC (<a title="obraz qcow2 64-bit Little Endian PowerPC dla OpenStack" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="obraz raw 64-bit Little Endian PowerPC dla OpenStack" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, zarówno jako obraz maszyny oraz przez AWS Marketplace.
          <ul class="quicklist downlist">
           <li><a title="Obrazy Maszyn Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Obrazy Maszyn Amazon</a></li>
           <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
          </ul>
      </li>
      <li>Microsoft Azure, Azure Marketplace.
          <ul class="quicklist downlist">
           <li><a title="Debian 12 w Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
           <li><a title="Debian 11 w Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
     </ul>
      </li>
    </ul>
</div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Kup zestaw płyt CD lub DVD od jednego z
      dystrybutorów płyt z Debianem</a></h2>

    <p>Wielu z dystrybutorów sprzedaje zestawy w cenie od kilkunastu do
    kilkudziesięciu złotych plus koszty
    wysyłki (informacje na temat tego, czy dany dystrybutor sprzedaje płyty
    za granicę można uzyskać na podanej stronie).<br>
    Płyty z dystrybucją są dołączone także do niektórych <a
    href="../doc/books">książek na temat Debiana</a>.</p>

   <p>Oto podstawowe zalety zestawu płyt:</p>

   <ul>
      <li>Instalacja z płyt jest łatwiejsza.</li>
      <li>Można zainstalować z nich system na maszynach nie
	podłączonych do internetu.</li>
      <li>Można zainstalować Debiana na dowolnej liczbie maszyn bez potrzeby
	  pobierania pakietów z internetu.</li>
      <li>Płyta CD może przydać się do ratowania systemu w razie jego
	uszkodzenia.</li>
   </ul>

    <h2><a href="pre-installed">Kup komputer z preinstalowanym
       Debianem</a></h2>
   <p>Takie rozwiązanie ma szereg zalet:</p>
   <ul>
      <li>Nie musisz instalować Debiana.</li>
      <li>Instalacja jest wstępnie skonfigurowana, tak by pasowała do sprzętu.</li>
      <li>Sprzedawca może oferować pomoc techniczną.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Wypróbuj <q>Debian live</q> przed zainstalowaniem</a></h2>
    <p>
      Możesz wypróbować Debiana uruchamiając system <q>live</q> z płyty CD, DVD
      lub napędu USB, bez instalowania żadnych plików w komputerze. Jeśli się
      zdecydujesz, możesz uruchomić zawarty instalator (począwszy od Debian 10 Buster 
      jest to przyjazny użytkownikowi <a href="https://calamares.io">Instalator Calamares</a>).             
      Zakładając, że odpowiada tobie rozmiar obrazu, język oraz wybór pakietów,
      ta metoda może być odpowiednia dla ciebie.
      Możesz przeczytać <a href="../CD/live#choose_live">więcej informacji na temat
      tej metody</a> aby podjąc decyzję.    
    </p>
    <ul class="quicklist downlist">
      <li><a title="Pobierz torrenty live dla 64-bitowego PC z procesorem Intel lub AMD"
	     href="<live-images-url/>/amd64/bt-hybrid/">torrenty live dla 64-bitowego PC</a></li>
    </ul>
  </div>

</div>


# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego 
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w
<a href="../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy instalacyjne dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>
